<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('users.index');
});

Route::resource('actions', 'actionsController');
Route::resource('campaign_actions', 'campaign_actionsController');
Route::resource('campaign_rewards', 'campaign_rewardsController');
Route::post('/submitCampaign', 'campaignsController@store');
//Route::get('campaigns/create', 'campaignsController@create');
//Route::get('campaigns', 'campaignsController@index');
Route::resource('campaigns', 'campaignsController');
Route::resource('memes', 'memesController');
Route::resource('products', 'productsController');
Route::resource('rewards', 'rewardsController');
Auth::routes();


Route::resource('user_types', 'user_typesController');
Route::resource('transactions', 'TransactionsController');
Route::resource('credits', 'CreditsController');

Route::get('auth/facebook',['as'=>'auth/facebook','uses'=>'Auth\LoginController@redirectToProvider']);
Route::get('auth/facebook/callback',['as'=>'auth/facebook/callback','uses'=>'Auth\LoginController@handleProviderCallback']);


Route::get('home', array('as' => 'home', 'uses' => function(){
  return view('users.index');
}));

Route::resource('profile', 'profileController');
Route::resource('settings','settingsController');
Route::resource('friend','friendController');

