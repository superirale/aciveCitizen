# config valid only for current version of Capistrano
#lock '3.6.1'
lock '3.8.0'

set :application, 'activecitizen'
set :repo_url, 'git@gitlab.com:superirale/aciveCitizen.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :branch, "master"
# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, '/var/www/html/tony/activecitizen'

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
set :pty, true
set :use_sudo, false
