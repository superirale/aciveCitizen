// Pure Javascript written on here...


//cloning of the take action section
var contentToClone = document.getElementsByClassName('clone');
var cloneContain = document.getElementById('cloneContainer')
var myClone;

function addClone(){
	for (var i = 0; i < contentToClone.length; i++) {
		myClone = contentToClone[contentToClone.length - 1].cloneNode(true);
	}
	cloneContain.appendChild(myClone);
}

//event listener
document.getElementById("cloneButton").addEventListener("click", addClone);


//readurl of text file
document.getElementById('wizard-picture').addEventListener('change', readURL, true);
function readURL(){
    var file = document.getElementById("wizard-picture").files[0];
    var reader = new FileReader();
    reader.onloadend = function(){
        document.getElementById('wizardPicturePreview').src = "" + reader.result;        
    }
    if(file){
        reader.readAsDataURL(file);
    }else{
    }
}



//JQUERY HERE.........

$(document).ready(function(){
	//medium clone begin here
	var editor = new Dante.Editor(
	  {
	    el: "#editor",
	    // debug: true
	  }
	);

	function dataURItoBlob(dataURI) {
	    var arr = dataURI.split(','), mime = arr[0].match(/:(.*?);/)[1];
	    return new Blob([atob(arr[1])], {type:mime});
	}

	function saveDante(el) {
		  // console.log( el.find(".section-inner").html() );
		  
		  var contents = $(el.find('.section-inner').html()),
		      images = contents.find('img');
		  
		  images.each(function (i, image) {
		    
		    // no way to get original name of photos?
		    // Check line 110 on
		    // https://github.com/michelson/Dante/blob/master/app/assets/javascripts/dante/tooltip_widgets/uploader.js.coffee
		    console.log( image );
		    
		    var blob = dataURItoBlob( image.src );
		    
		    console.log( blob );
		  });
		}
		    
		editor.start();

		$('.save').click(function(e) {
		  e.preventDefault();
		  saveDante( $('#editor') );
	});


});