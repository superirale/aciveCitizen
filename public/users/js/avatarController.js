
//readurl of text file
document.getElementById('id_avatar').addEventListener('change', readURL, true);
//delete whatever photo and return default
document.getElementById('delete-avatar-link').addEventListener('click', deleteUrl, true);
//click on button and simulate click on label
document.getElementById('avatar-button').addEventListener('click', clickLabel, true);

function readURL(){
    var file = document.getElementById("id_avatar").files[0];
    var reader = new FileReader();
    reader.onloadend = function(){
    	//console.log(reader.result);
        document.getElementsByClassName('avatar_label')[0].style.backgroundImage = "url("+ reader.result +")"  
        //no-repeat scroll center center / 100% auto;    
    }
    if(file){
        reader.readAsDataURL(file);
    }else{
    }
}

function deleteUrl(){
	var confirmation = confirm('Are you sure you want to delete your avatar?');
	if (confirmation){
		document.getElementsByClassName('avatar_label')[0].style.backgroundImage = "url('img/Avaters/default-avatar.png')"   
	}
}

function clickLabel(){
	document.getElementsByClassName('avatar_label')[0].click();
}


