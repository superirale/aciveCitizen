var swiper = new Swiper('.swiper-container', {
        nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        paginationClickable: true,
        centeredSlides: true,
        autoplay: 3500,
        autoplayDisableOnInteraction: false
 });

var swipered = new Swiper('.swiper-contained', {
	speed: 30000,
    slidesPerView: 5,
    slidesPerGroup: 5,
    autoplay: 3500,
    loop: true,
    centeredSlides: true,
    setWrapperSize: true,
    lazyLoading: true,
    slidesOffsetBefore: -10,
    breakpoints: {
	    // when window width is <= 480px
	    480: {
	      slidesPerView: 2,
	      spaceBetweenSlides: 10,
	      speed: 30000,
	      autoplay: 3500,
	      loop: true,
	      centeredSlides: true,
	      setWrapperSize: true,
	      lazyLoading: true
	    }
	}
});