var options = {
  valueNames: [ 'reward-gallery', 'story-description' ],
  page: 3,
  plugins: [
	  ListPagination({})
	]
};

var userList = new List('gallery-list', options);