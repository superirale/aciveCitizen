var FormField = function(name, regex) {
    this.name = name;
    this.regex = regex;
};

var form = [
   // new FormField('name', /^[a-zA-Z ]+$/),
    new FormField('email', /^[a-zA-Z0-9_]{1,}@[a-z0-9]{1,}\.[a-z]{1,}$/)
    //new FormField('phone', /^[0-9]{9}$/),
    //new FormField('birthday', /^([0|1|2][0-9]|3[01])\/(0[0-9]|1[012])\/[0-9]{4}$/)
];

var validateForm = function() {

    var valid = true;
    var toggleError;

    for (var i = 0; i < form.length; i++) {
        toggleError = 'none';
        if (!isValidField(form[i])) {
            valid = false;
            toggleError = 'block';
        }
        toggleFieldError(form[i].name, toggleError);
    }

    if (valid) {
        formIsValid();
    }
};

var isValidField = function(formField) {
    return formField.regex.test(
        getFormFieldValue(formField.name)
    );
};

var getFormFieldValue = function(name) {
    return document.getElementsByName(name)[0].value;
};

var toggleFieldError = function(name, visibility) {
    document.getElementById('error-' + name).style.display = visibility;
};

var formIsValid = function(){
    document.getElementById('loading').style.display = 'block';
    //$("#signInModal").modal('hide');
    document.getElementById('myForm').submit();    
}