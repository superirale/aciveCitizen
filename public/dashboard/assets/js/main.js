(function ($) {
    $.fn.extend({
        esRePosition: function (options) {
            var defaults = {
                replica: "replica",
                input: "input"
            };
            options = $.extend(defaults, options);
            return this.each(function () {
                //Plugin options
                var o = options;

                //Main object
                var $mask = $(this);
                var $id = $mask.attr("id");

                var $replica = $("#" + $id + "_" + $mask.data("replica"));
                if ($replica.length === 0) {
                    $replica = $("#" + $id + "_" + o.replica);
                }
                if ($replica.length > 0) {
                    var $img_replica = $("img", $replica);
                    $img_replica.height($replica.height());
                }
                var $input = $("#" + $id + "_" + $mask.data("input"));
                if ($input.length === 0) {
                    $input = $("#" + $id + "_" + o.input);
                }


                var $img = $("img", $mask);
                var $mask_h = $mask.height();
                var $mask_w = $mask.width();
                var $photo_org_h = $img.height();
                var $photo_org_w = $img.width();
                $img.height($mask_h);
                var $photo_h = $img.height();
                var $photo_w = $img.width();
                var $max_margin = ($photo_w - $mask_w) * -1;
                $img.draggable({
                    axis: "x",
                    cursor: "move",
                    drag: function (event, ui) {
                        $leftmarginpx = ui.position.left;
                        if ($leftmarginpx > 0) {
                            ui.position.left = 0;
                        }
                        if ($leftmarginpx < $max_margin) {
                            ui.position.left = $max_margin;
                        }
                        $leftmarginpx = ui.position.left;
                        $leftmarginpercent = ($leftmarginpx * 100 / $photo_w);
                        $newleftmarginpx = ($leftmarginpercent * $("img", $replica).width() / 100);
                        $("img", $replica).css("margin-left", $newleftmarginpx + "px");
                        $input.val($newleftmarginpx + "px");
                    }
                });

                $(window).load(function () {

                });

            });
        }
    });
})(jQuery);

(function(){
//type of colour
type = ['','info','success','warning','danger'];
var greetings;

app = {
    initGreetings: function(){
        var time = new Date().getHours();

        if (time < 12) {
          greetings = 'Good Morning';
        }
        else if (time >= 12 && time < 16 ) {
          greetings = 'Good Afternoon';
        }
        else if(time >= 16){
          greetings = 'Good Evening';
        }
        return greetings;
    },
    initPickColor: function(){
        $('.pick-class-label').click(function(){
            var new_class = $(this).attr('new-class');
            var old_class = $('#display-buttons').attr('data-class');
            var display_div = $('#display-buttons');
            if(display_div.length) {
                var display_buttons = display_div.find('.btn');
                display_buttons.removeClass(old_class);
                display_buttons.addClass(new_class);
                display_div.attr('data-class', new_class);
            }
        });
    },

    initChartist: function(){

        var dataSales = {
            labels: ['9:00AM', '12:00AM', '3:00PM', '6:00PM', '9:00PM', '12:00PM', '3:00AM', '6:00AM'],
            series: [
               [287, 385, 490, 492, 554, 586, 698, 695, 752, 788, 846, 944],
              [67, 152, 143, 240, 287, 335, 435, 437, 539, 542, 544, 647],
              [23, 113, 67, 108, 190, 239, 307, 308, 439, 410, 410, 509]
            ]
        };

        var optionsSales = {
            lineSmooth: false,
            low: 0,
            high: 800,
            showArea: true,
            height: "245px",
            axisX: {
              showGrid: false,
            },
            lineSmooth: Chartist.Interpolation.simple({
              divisor: 3
            }),
            showLine: false,
            showPoint: false,
        };

        var responsiveSales = [
            ['screen and (max-width: 640px)', {
              axisX: {
                labelInterpolationFnc: function (value) {
                  return value[0];
                }
              }
            }]
        ];

        Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);


        var data = {
            labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
            series: [
              [542, 443, 320, 780, 553, 453, 326, 434, 568, 610, 756, 895],
              [412, 243, 280, 580, 453, 353, 300, 364, 368, 410, 636, 695]
            ]
        };

        var options = {
            seriesBarDistance: 10,
            axisX: {
                  showGrid: false
            },
            height: "245px"
        };

        var responsiveOptions = [
            ['screen and (max-width: 640px)', {
              seriesBarDistance: 5,
              axisX: {
                labelInterpolationFnc: function (value) {
                  return value[0];
                }
              }
            }]
        ];

        Chartist.Bar('#chartActivity', data, options, responsiveOptions);

        var dataPreferences = {
            series: [
                [25, 30, 20, 25]
            ]
        };

        var optionsPreferences = {
            donut: true,
            donutWidth: 40,
            startAngle: 0,
            total: 100,
            showLabel: false,
            axisX: {
                showGrid: false
            }
        };

        Chartist.Pie('#chartPreferences', dataPreferences, optionsPreferences);

        Chartist.Pie('#chartPreferences', {
            labels: ['62%','32%','6%'],
            series: [62, 32, 6]
        });
    },

    initChartjs: function(){
        var ctx = document.getElementById("myBarChart");
        var barChartdata = {
            labels: ["January", "February", "March", "April", "May", "June", "July"],
            datasets: [
                {
                    label: "My First dataset",
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1,
                    data: [65, 59, 80, 81, 56, 55, 40]
                }
            ]
        };
        var barChartdata2 = {
            labels: ["August", "September", "October", "November", "December"],
            datasets: [
                {
                    label: "My Second dataset",
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)'
                    ],
                    borderWidth: 1,
                    data: [65, 59, 80, 81, 56]
                }
            ]
        };
        var options=  {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero:true
                  }
              }]
          }
        }

        var myBarChart = new Chart(ctx, {
            type: 'bar',
            data: barChartdata,
            options: options
        });


        function a() {
            myBarChart = new Chart(ctx, {
            type: 'bar',
            data: barChartdata,
            options: options
          });
        }

        function b() {
            myBarChart = new Chart(ctx, {
            type: 'bar',
            data: barChartdata2,
            options: options
          });
        }


        $("#toggle").on("change", function(){
            myBarChart.destroy();
            return (this.tog = !this.tog) ? a() : b();
        });

      },

    initCirclijs: function(){

        $(window).on('resize', function(){
            $("#circli").html('');
            $("#circli").circliful();
        });

        setInterval(function() {
          $("#circli").html('');
          $("#circli").circliful(); // Do something every 5 seconds
        }, 5000);


    },

    initFlot: function(){
        var d1 = [
          [0, 950], [1, 1300], [2, 1600], [3, 1900], [4, 2100], [5, 2500], [6, 2200], [7, 2000], [8, 1950], [9, 1900], [10, 2000], [11, 2120]
        ];
        var d2 = [
            [0, 450], [1, 500], [2, 600], [3, 550], [4, 600], [5, 800], [6, 900], [7, 800], [8, 850], [9, 830], [10, 1000], [11, 1150]
        ];

        var tickArray = ['Janv', 'Fev', 'Mars', 'Apri', 'May', 'June', 'July', 'Augu', 'Sept', 'Nov'];

        /****  Line Chart  ****/
        var graph_lines = [{
            label: "Line 1",
            data: d1,
            lines: {
                lineWidth: 2
            },
            shadowSize: 0,
            color: '#0090D9'
        }, {
            label: "Line 1",
            data: d1,
            points: {
                show: true,
                fill: true,
                radius: 6,
                fillColor: "#0090D9",
                lineWidth: 3
            },
            color: '#fff'
        }, {
            label: "Line 2",
            data: d2,
            animator: {
                steps: 300,
                duration: 1000,
                start: 0
            },
            lines: {
                fill: 0.7,
                lineWidth: 0,
            },
            color: '#18A689'
        }, {
            label: "Line 2",
            data: d2,
            points: {
                show: true,
                fill: true,
                radius: 6,
                fillColor: "#18A689",
                lineWidth: 3
            },
            color: '#fff'
        }, ];

        function lineCharts(){
            var line_chart = $.plotAnimator($('#graph-lines'), graph_lines, {
                xaxis: {
                    tickLength: 0,
                    tickDecimals: 0,
                    min: 0,
                    ticks: [
                        [0, 'Jan'], [1, 'Fev'], [2, 'Mar'], [3, 'Apr'], [4, 'May'], [5, 'Jun'], [6, 'Jul'], [7, 'Aug'], [8, 'Sept'],  [9, 'Oct'], [10, 'Nov'], [11, 'Dec']
                    ],
                    font: {
                        lineHeight: 12,
                        weight: "bold",
                        family: "Open sans",
                        color: "#8D8D8D"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    tickColor: "#f3f3f3",
                    font: {
                        lineHeight: 13,
                        weight: "bold",
                        family: "Open sans",
                        color: "#8D8D8D"
                    }
                },
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderColor: "transparent",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 15,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius: 4
                },
                legend: {
                    show: false
                }
            });
        }
        lineCharts();

        /****  Bars Chart  ****/
        var graph_bars = [{
            // Visitors
            data: d1,
            color: '#00b5f3'
        }, {
            // Returning Visitors
            data: d2,
            color: '#008fc0',
            points: {
                radius: 4,
                fillColor: '#008fc0'
            }
        }];

        function barCharts(){
            bar_chart = $.plotAnimator($('#graph-bars'), graph_bars, {
                series: {
                    bars: {
                        fill: 1,
                        show: true,
                        barWidth: .6,
                        align: 'center'
                    },
                    shadowSize: 0
                },
                xaxis: {
                    tickColor: 'transparent',
                    ticks: [
                        [0, 'Jan'], [1, 'Fev'], [2, 'Mar'], [3, 'Apr'], [4, 'May'], [5, 'Jun'], [6, 'Jul'], [7, 'Aug'], [8, 'Sept'], [9, 'Oct'], [10, 'Nov'], [11, 'Dec']
                    ],
                    font: {
                        lineHeight: 12,
                        weight: "bold",
                        family: "Open sans",
                        color: "#9a9a9a"
                    }
                },
                yaxis: {
                    ticks: 3,
                    tickDecimals: 0,
                    tickColor: "#f3f3f3",
                    font: {
                        lineHeight: 13,
                        weight: "bold",
                        family: "Open sans",
                        color: "#9a9a9a"
                    }
                },
                grid: {
                    backgroundColor: {
                        colors: ["#fff", "#fff"]
                    },
                    borderColor: "transparent",
                    margin: 0,
                    minBorderMargin: 0,
                    labelMargin: 15,
                    hoverable: true,
                    clickable: true,
                    mouseActiveRadius: 4
                },
                legend: {
                    show: false
                }
            });
        }

        $("#graph-lines").on("animatorComplete", function () {
            $("#lines, #bars").removeAttr("disabled");
        });

        $("#lines").on("click", function () {
            $('#bars').removeClass('active');
            $('#graph-bars').fadeOut();
            $(this).addClass('active');
            $("#lines, #bars").attr("disabled", "disabled");
            $('#graph-lines').fadeIn();
            lineCharts();
        });

        $("#graph-bars").on("animatorComplete", function () {
            $("#bars, #lines").removeAttr("disabled")
        });

        $("#bars").on("click", function () {
            $("#bars, #lines").attr("disabled", "disabled");
            $('#lines').removeClass('active');
            $('#graph-lines').fadeOut();
            $(this).addClass('active');
            $('#graph-bars').fadeIn().removeClass('hidden');
            barCharts();
        });

        $('#graph-bars').hide();

        function showTooltip(x, y, contents) {
            $('<div id="flot-tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y + 5,
                left: x + 5,
                color: '#fff',
                padding: '2px 5px',
                'background-color': '#717171',
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
        };

        $("#graph-lines, #graph-bars").bind("plothover", function (event, pos, item) {
            $("#x").text(pos.x.toFixed(0));
            $("#y").text(pos.y.toFixed(0));
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;
                    $("#flot-tooltip").remove();
                    var x = item.datapoint[0].toFixed(0),
                        y = item.datapoint[1].toFixed(0);
                    showTooltip(item.pageX, item.pageY, y + " visitors");
                }
            } else {
                $("#flot-tooltip").remove();
                previousPoint = null;
            }
        });

    },

    initGoogleMaps: function(){
      var myLatlng = new google.maps.LatLng(40.748817, -73.985428);
      var mapOptions = {
        zoom: 13,
        center: myLatlng,
        scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
        styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]

      }
      var map = new google.maps.Map(document.getElementById("map"), mapOptions);

      var marker = new google.maps.Marker({
          position: myLatlng,
          title:"Hello World!"
      });

      // To add the marker to the map, call setMap();
      marker.setMap(map);
    },

    showNotification: function(from, align){

    	color = Math.floor((Math.random() * 4) + 1);
      //initiate greetings- note this wll return greetings
    	this.initGreetings();

    	$.notify({
        	icon: "icon-trophy",
        	message: greetings + ", Welcome to <b>Active Citizen Dashboard</b>"

        },{
            type: type[color],
            timer: 4000,
            placement: {
                from: from,
                align: align
            }
        });
    },
    initIntro: function(){
        var $this = this;
        function startIntro(){
            var intro = introJs();
              intro.setOptions({
                showBullets : true,
                steps: [
                  {
                    element: '#sidebar', position: "right",
                    intro: "This menu permit you to navigate through all pages."
                  },{
                    element: '#search-area', position: "left",
                    intro: "hit us with your search and researches."
                  },{
                    element: '.dashboard-stat2:first-child', position: "bottom",
                    intro: "Here you can view number of your visits campaigns."
                  },{
                    element: '#flot-container', position: "top",
                    intro: "current vs recent campaigns"
                  },{
                    element: '.col-md-4', position: "right",
                    intro: "campaigns performances."
                  },{
                    element: '.col-md-8', position: "left",
                    intro: "Rewards distribution"
                  }
                ]
              });

            intro.start();
            $("html, body").scrollTop(0);
            intro.oncomplete(function() {
                $("html, body").scrollTop(0);
                //greetings after all intro.....
                $this.showNotification('top', 'right');
                $this.initChartist();
                $this.initChartjs();
                $this.initCirclijs();
                $this.initFlot();
                $this.counter();
            });
            intro.onexit(function() {
                $("html, body").scrollTop(0);
                //greetings after all intro.....
                $this.showNotification('top', 'right');
                $this.initChartist();
                $this.initChartjs();
                $this.initCirclijs();
                $this.initFlot();
                $this.counter();
            });
        }

        startIntro();

        /* Replay Intro on Button click
        $('#start-intro').click(function () {
            startIntro();
        });*/
    },
    wizard: function(){
        $('#rootwizard').bootstrapWizard({});

        $("#publish").click(function(){
            //$('#rootwizard').bootstrapWizard('back')
        });
    },

    cloneHandler: function(){
          // Pure Javascript written on here...
          //cloning of the take action section
          var childClones = document.getElementsByClassName('clone');
          var cloneParent = document.getElementById('cloneContainer');
          var cloneButton = document.getElementsByClassName('cloneButton')[0];
          var removeChildClone = document.getElementsByClassName('remove')[0];
          var newClones;
          //console.log(cloneParent.children.length);

          function addClone(){
              for (var i = 0; i < childClones.length; i++) {
                  newClones = childClones[childClones.length - 1].cloneNode(true);
                  newClones.classList.add('slideInDown');
                  newClones.querySelector('textarea').value = '';
              }
              cloneParent.appendChild(newClones);
              newClones.getElementsByClassName('cloneButton')[0].addEventListener("click", addClone);
              newClones.getElementsByClassName('remove')[0].addEventListener("click", removeClone);
              // console.log(cloneParent.children.length);
          }

          function removeClone(){
              if (cloneParent.children.length == 1) {
                  this.parentNode.querySelector('textarea').value = '';
              }
              else{
                  this.parentNode.remove();
              }
          }

          //event listener
          cloneButton.addEventListener("click", addClone);
          removeChildClone.addEventListener("click", removeClone);
    },

    mediumHandler: function(){
          // medium clone begin here
          var editor = new Dante.Editor(
            {
              el: "#editor",
              // debug: true
            }
          );

          function dataURItoBlob(dataURI) {
              var arr = dataURI.split(','), mime = arr[0].match(/:(.*?);/)[1];
              return new Blob([atob(arr[1])], {type:mime});
          }

          function saveDante(el) {
                // console.log( el.find(".section-inner").html() );

                var contents = $(el.find('.section-inner').html()),
                    images = contents.find('img');

                images.each(function (i, image) {

                  // no way to get original name of photos?
                  // Check line 110 on
                  // https://github.com/michelson/Dante/blob/master/app/assets/javascripts/dante/tooltip_widgets/uploader.js.coffee
                  //console.log( image );

                  var blob = dataURItoBlob( image.src );

                  //console.log( blob );
                });
          }

          editor.start();

          $('.save').click(function(e) {
                e.preventDefault();
                saveDante( $('#editor') );
          });
    },

    readFile: function(){
        document.getElementById('wizard-picture').addEventListener('change', readURL, true);
        function readURL(){
            var file = document.getElementById("wizard-picture").files[0];
            var reader = new FileReader();
            reader.onloadend = function(){
                document.getElementById('wizardPicturePreview').src = "" + reader.result;
            }
            if(file){
                reader.readAsDataURL(file);
            }
            else{
            }
        }
    },

    selectPicker: function(){
        $('.selectpicker').selectpicker();
    },

    searchToggler: function(){

        $("#search-area").on("focus", function(){
            $(this).css("width", "300px")
        });

        $("#search-area").on("blur", function(){
            $(this).css("width", "200px")
        });
    },

    toggleIconsHandler: function(){

        $('.showIcons').on("click", function(){
            $(".icons-list").fadeToggle('slow')
            //$("#icons-list").fadeout('slow');
        })

        $('.icons-list a').on("click", function(){
            $(".icons-list").fadeOut();
        });
    },

    anotherCloneHandler: function(){
        var messages = {};
        var twitterClones = document.getElementsByClassName('clone');
        var cloneParent = document.getElementById('cloneContainer');
        var twitterButton = document.getElementById('twitter');


        function addClone(){
            for (var i = 0; i < twitterClones.length; i++) {
                messages.twitter = twitterClones[twitterClones.length - 1].cloneNode(true);
                messages.twitter.classList.add('fadeIn');
                messages.twitter.style.display = 'block';
                //add event listener to all the new clones
                messages.twitter.getElementsByClassName('remove')[0].addEventListener("click", removeClone);
                //console.log(messages.twitter.getElementsByClassName('remove')[0]);
            }
            cloneParent.appendChild(messages.twitter).scrollIntoView({block: "start", behavior: "smooth"});
        }

        function removeClone(){
            this.parentNode.remove();
        }


        twitterButton.addEventListener("click", addClone);
    },

    clonesHandler: function(){
          //this is use to store array .......['twitter', 'facebook', 'google']
          var messages = [];

          var clones = document.getElementsByClassName('clone');
          var cloneParent = document.getElementsByClassName('cloneContainer')[0];
          var twitterButton = document.getElementsByClassName('twitter')[0];
          var facebookButton = document.getElementsByClassName('facebook')[0];
          var instagramButton = document.getElementsByClassName('instagram')[0];
          var petitionButton = document.getElementsByClassName('petition')[0];
          var emailButton = document.getElementsByClassName('email')[0];


          function addClone(){
            /* note if this loop stays outside this function, you can only add any of the clones just once */
            for (var i = 0; i < clones.length; i++) {
              messages[i] = clones[i].cloneNode(true);
              messages[i].classList.add('fadeIn');
              messages[i].style.display = 'block';
              //add event listener to all the new clones
              messages[i].getElementsByClassName('remove')[0].addEventListener("click", removeClone);
              //console.log(messages.twitter.getElementsByClassName('remove')[0]);
          }

          switch(this.className){
              case (twitterButton.className):
                cloneParent.appendChild(messages[0]).scrollIntoView({block: "start", behavior: "smooth"});
                break;
              case (facebookButton.className):
                cloneParent.appendChild(messages[1]).scrollIntoView({block: "start", behavior: "smooth"});
                break;
              case (instagramButton.className):
                cloneParent.appendChild(messages[2]).scrollIntoView({block: "start", behavior: "smooth"});
                break;
              case (petitionButton.className):
                cloneParent.appendChild(messages[3]).scrollIntoView({block: "start", behavior: "smooth"});
                break;
              case (emailButton.className):
                cloneParent.appendChild(messages[4]).scrollIntoView({block: "start", behavior: "smooth"});
                break;
            }
          }

          function removeClone(){
            this.parentNode.remove();
            /*var confirmation = confirm("Are you sure about this")
            if(confirmation){

              this.parentNode.remove();
            }*/
          }

          //add event listener to all buttons....
          twitterButton.addEventListener("click", addClone);
          facebookButton.addEventListener("click", addClone);
          instagramButton.addEventListener("click", addClone);
          petitionButton.addEventListener("click", addClone);
          emailButton.addEventListener("click", addClone);


    },

    campaignsHandler: function(){

        //variables for possible time/ occassion
        var thisMonth, thisDate, thisDay, thisHour, thisMinute;

        // retrievedata from local storage
        var retrievedData = localStorage.getItem("whole");

        //old object for use incase there's is one...{}
        var oldCampaign = JSON.parse(retrievedData);

        //function to get present day, time, hour
        function CurrentDate(){
          var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
          var dayNames= ["Sun","Mon","Tue","Wed","Thur","Fri","Sat"];
          var date = new Date();
          //date.setDate(date.getDate() + 1);
          var day = date.getDate();
          var month = date.getMonth();
          var hours = date.getHours();
          var minutes = date.getMinutes();
          var ampm = hours >= 12 ? 'pm' : 'am';
          hours = hours % 12;
          hours = hours ? hours : 12; // the hour '0' should be '12'
          minutes = minutes < 10 ? '0'+minutes : minutes;
          thisMonth = monthNames[month];
          thisDate = day;
          thisDay = dayNames[date.getDay()];
          thisHour = hours;
          thisMinute = minutes;
        }

        CurrentDate();

        //our campaigns is either old campaign or new one
        var campaigns = oldCampaign || {};

        //number of users in campaign old or new......must be an array
        campaigns.users = campaigns.users || [];

        if ($('body').data('page') == 'view_campaigns') {
          // retrievedata from local storage
          var retrievedReward = localStorage.getItem("Reward");

          //get reward data ready for use........
          var RewardData = JSON.parse(retrievedReward) || {};
          RewardData.users = RewardData.users || [];


          for (var i = 0; i < RewardData.users.length; i++) {
            //RewardData.users[i].rewardsClone
            $('.modal-body .row').append(RewardData.users[i].rewardsClone);
          }

          $('.editReward').bind('click', function(){
              $('#events-table .button.active').text($(this).find('h4').text());
              campaigns.saveCampaign();
          });
        }

        //if I should click on edit button and there's a user index to edit do this
        if (campaigns.shouldIedit === true && campaigns.userIndex !== "") {
          //get user
          var user = campaigns.users[campaigns.userIndex];
          // a new textarea array to store our textarea values
          var textareas = [];

          //change title to current edited
          $("#campaign_title").html(user.header);
          //change image source to current user source
          $("#wizardPicturePreview").attr("src", user.banner);
          //load editor content
          $("#editor").html(user.editor);

          // fill our cloneContainer with social media in user
          $(".cloneContainer").html(user.media);

          // for some reasons the textareasand input are empty, fill them up with their contents
          $('.cloneContainer').find(".fadeIn").find(".form-control").each(function (index, value) {
              $(this).val(user.text[index]);
          });

          // add event listerner to this again..I guess the previous isn't in the DOM
          $(".cloneContainer").find(".remove").bind("click", function(){
            $(this).parent().remove();
          });

          //CHANGE #publish id to saveUser to prevent it from adding to users instead of editing user
          $("#publish").attr("id", "saveUser");

          //on click on saveUser button change user keys
          $("#saveUser").on("click", function(){
            // gather all the content of the textareas and input available
            $('.cloneContainer').find(".fadeIn").find(".form-control").each(function () {
              textareas.push($(this).val());
            });
            // image source equals the current photo source
            user.banner =  $("#wizardPicturePreview").attr("src");
            user.header = $('#editor').find("h3").clone().text();
            var $fullrow = '<td><a href="add_campaign.html" class="c-red"> ' + user.header +' </a></td><td>Social</td><td><a class="btn btn-dark" data-toggle="modal" href="#modal-id">Add</a></td><td class="text-center"><div class="event-date"><div class="event-month"><span class="dots">&nbsp;</span><span title=" ' + thisMonth + ' "> ' + thisMonth + '</span><span class="dots">&nbsp;</span></div><div class="event-day"> ' + thisDate + '</div><div class="event-day-txt"> ' + thisDay + '</div></div></td><td class="text-center"><span class="label label-success w-300">Active</span></td><td class="text-center"> <a href="add_campaign.html" data-rel="tooltip" title="Edit this Event" class="edit btn btn-sm btn-icon btn-rounded btn-default"><i class="fa fa-pencil"></i></a><a href="#" data-rel="tooltip" title="Delete this Event" class="delete btn btn-sm btn-icon btn-rounded btn-default"><i class="fa fa-times"></i></a></td>'
            user.image = '<img src=" ' +  user.banner + ' " alt="mars" class="img-responsive" />';
            user.main = '<tr><td><a href="add_campaign.html" class="magnific" title="Bruno Mars"> ' + user.image + '</a></td> ' + $fullrow  + ' </tr>';
            user.media = $('.cloneContainer').clone().html();
            user.editor = $("#editor").clone().html();
            user.text = textareas;
            campaigns.saveCampaign();
            campaigns.updateCampaign();
            window.location.href = "campaign.html";
          });

        }

        var $container = $(".page-notes"),
        $campaignList = $container.find('#campaigns'),
        $noData = $container.find('#no-data'),
        $yesData = $container.find('#yes-data'),
        $table = $campaignList.find("#events-table"),
        $tableBody = $table.find("tbody"),
        $addCampaign = $container.find('#publish'),
        $editRow = $tableBody.find("table .edit"),
        $deleteRow = $tableBody.find("table .delete");

        var campaignsParameters = {
          shouldIedit: false,
          userIndex: '',
          allUsers: campaigns.users.length,
          deletedUser: 0,
          //lets add functions
          showData: function(){
            if ($tableBody.length > 0) {
                $yesData.css("display", "block");
                $noData.css("display", "none");
            }
            else{
                $yesData.css("display", "none");
                $noData.css("display", "block");
            }
          },
          deleteCampaign: function(){
            // console.log($tableBody.find(".delete"));
            /*$tableBody.find(".delete").bind("click", function(){
                $(this).parent().parent().remove();
                campaigns.users.splice(i, 1);
                alert("This is gone " + i);
              }); */
          },
          updateCampaign: function(){
            for (var i = 0; i < campaigns.users.length; i++) {
              $tableBody.append(campaigns.users[i].main);
            }
            this.shouldIedit = false;
            this.userIndex = "";
            //this.showData();
            this.saveCampaign();
          },

          addCampaign : function(){
            var $editor = $('#editor').clone().html();
            var $header = $('#editor').find("h3").clone().text();
            var $paragraph = $("editor").find("p").clone().text();
            var $banner = $(".picture").find("img#wizardPicturePreview").clone();
            var $cloneContainer = $('.cloneContainer').clone().html();
            var textareas = [];

            $('.cloneContainer').find(".fadeIn").find(".form-control").each(function () {
                textareas.push($(this).val());

            });


            var campaignActions = {};

            $('.cloneContainer').find(".fadeIn").each(function () {
                name = $(this).find('h5').text().toLowerCase();
                fields = {};
                cnt = 0;
                fields[cnt] = {};

                $(this).find(".form-control").each(function () {
                    fields[cnt][$(this).attr('name')] = $(this).val();
                });

                campaignActions[name] = fields[cnt];

                cnt += 1;
            });

            var cData = new FormData();
            cData.append('campaign_image_reference', $('#wizard-picture').prop('files')[0]);
            cData.append('description', $paragraph);
            cData.append('title', $header);
            cData.append('campaign_action_data', JSON.stringify(campaignActions));

            $.ajax({
                url: '/campaigns',
                data: cData,
                contentType: false,
                cache:false,
                type: 'POST',
                processData: false,
                sucess: function (resp) {
                   console.log(resp.status);
                }
            });
            // event.preventDefault();
            window.location.href = "/campaigns";

          },
          saveCampaign: function(e){
            localStorage.setItem("whole", JSON.stringify(campaigns));
          },
          editUser: function(userdata){
            var $editor = $('#editor');
            var $header = $editor.find("h3");
            var $paragraph = $editor.find("p");
            var $banner = $(".picture").find("img#wizardPicturePreview");
            var $cloneContainer = $('#cloneContainer');

            //console.log(userdata);
          },
          editCampaign: function(){
            $($editRow).each(function (index, value) {
                $(this).bind("click", function(){
                //THIS RETURN USER INFO.....
                campaigns.users[index].getUser();
              });
            });
          }

        };

        $.extend(campaigns, campaignsParameters);


        $addCampaign.on('click', function(){
          campaigns.addCampaign();
        });


        if (campaigns.users.length > 0) {
          $("#total_users").html(campaigns.allUsers);
          $("#deleted_user").html(campaigns.deletedUser);
          //$("#deleted_user").html(campaigns.deletedUser);
          campaigns.updateCampaign();
          campaigns.showData();
        }

        var opt = {};
        // Tools: export to Excel, CSV, PDF & Print
        opt.sDom = "<'row m-t-10'<'col-md-6'><'col-md-6'T>r>t<'row'<'col-md-6'><'col-md-6 align-right'p>>",
        opt.oLanguage = { "sSearch": "","sZeroRecords": "No event found" } ,
        opt.iDisplayLength = 6,
        opt.oTableTools = {
            "sSwfPath": "assets/plugins/datatables/swf/copy_csv_xls_pdf.swf",
            "aButtons": []
        };
        opt.aaSorting = [[ 5, 'asc' ]];
        opt.aoColumnDefs = [
              { 'bSortable': false, 'aTargets': [0] }
           ];

        var oTable = $('#events-table').dataTable(opt);
        oTable.fnDraw();

        //campaigns.editCampaign();
        $("#events-table a.edit").each(function(index, value){
          $(this).bind('click', function (e) {
            e.preventDefault();
            if (confirm("Are you sure to edit this campaign ?") == false) {
                return;
            }
            var nRow = $(this).parents('tr')[0];

            campaigns.shouldIedit = true;
            campaigns.userIndex = index;
            campaigns.saveCampaign();
            window.location.href = "add_campaign.html";
          });
        });

        //Selecting rewards logic INSIDE THE MODAL
        $("#events-table .button").each(function(index, value){
          //$(this).removeClass('active');
          $(this).bind('click', function (e) {
            e.preventDefault();
             //modal index
            var modalIndex;
            modalIndex = $(this).parents('tr').index();
            $(this).addClass('active');
            //modal title equals present campaign header
            $(".modal-title").html(campaigns.users[modalIndex].header);

          });
        });

        //when modal is hidden remove class from btn
        $('#modal-id').on('hidden.bs.modal', function () {
          $("#events-table .button").removeClass('active');
        });

        $("#events-table a.delete").each(function(index, value){
          $(this).bind('click', function (e) {
            e.preventDefault();
            if (confirm("Are you sure to delete this campaign ?") == false) {
                return;
            }
            var nRow = $(this).parents('tr')[0];

            oTable.fnDeleteRow(nRow);
            // campaigns.users.splice(index, 1);
            // campaigns.deletedUser += 1;
            // campaigns.saveCampaign();
            // campaigns.showData();
            var cmp_id = parseInt($(this).data('payload'));
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                    url: '/campaigns/'+ cmp_id,
                    contentType: false,
                    cache:false,
                    type: 'DELETE',
                    processData: false,
                    sucess: function (resp) {
                       console.log(resp.status);
                    }
            });
            event.preventDefault();
            // window.location.href = "/campaigns";
          });
        });


    },

    rewardsHandler: function(){
        //the container to add our clones into....
        var $rewardsContainer = $("#rewards_container");
        //a clone of the object will be adding to container
        var $rewardsClone = $(".reward_clone").clone(true, true);
        var $rewardTitle = $rewardsClone.find("h4.title");
        var $rewardPoint = $rewardsClone.find("span.points");
        var $rewardDate = $rewardsClone.find("span.dates");
        //var $rewardTime = $rewardsClone.find("span.time");
        var $rewardLocation = $rewardsClone.find("span.locations");
        var $rewardImage = $rewardsClone.find("img");
        var $rewardHistory = $rewardsClone.find("i.fa");
        //var $editButtoncloned = $rewardsClone.find(".editReward");


        //picking inputs made by users
        var $rewardTitleInput = $("#reward_title");
        var $rewardPointInput = $("#reward_point");
        var $rewardLocationInput = $("#reward_location");
        var $rewardDateInput = $("#reward_date");
        var $rewardTimeInput = $("#reward_time");
        var $imageInputSource = $(".picture").find("img");
        var $submitButton = $("#submit_button");

        //edit tags/buttons clicked
        var $editButton = $rewardsContainer.find(".editReward");

        // retrievedata from local storage if it exist
        var retrieveData = localStorage.getItem("Reward");

        //old object for use incase there's is one...{}
        var oldRewards = JSON.parse(retrieveData);



        var Rewards = oldRewards || {};

        Rewards.users = Rewards.users || [];


        //Rewards Details pages logic here ......
        if (Rewards.users.length > 0 && Rewards.edit === true) {
          //get the user returned ready for editing
          var user = getReward(Rewards.userIndex);
          // LET PICK ALL THE ELEMENT IN THE DETAIL PAGE
          var $rewards_details = $("#rewards_details");
          var $image = $rewards_details.find("img");
          var $heading = $rewards_details.find("h4.title");
          var $point = $rewards_details.find("span.points");
          var $time = $rewards_details.find("span.time");
          var $date = $rewards_details.find("span.dates");
          var $location = $rewards_details.find("span.locations");
          //LET PICK ALL THE VALUE FROM THE REWARD USER
          $heading.html($(user.rewardsClone).find("h4.title").text());
          $point.html($(user.rewardsClone).find("span.points").text());
          $time.html($(user.rewardsClone).find("span.time").text());
          $date.html($(user.rewardsClone).find("span.dates").text());
          $time.html(user.time);
          $location.html($(user.rewardsClone).find("span.locations").text());
          $image.attr("src", $(user.rewardsClone).find("img").attr('src'));
          //return these terms to their default state..........
          Rewards.edit = false;
          Rewards.userIndex = '';
          saveRewards();

        }

        var RewardsParameters = {
          edit: false,
          userIndex: '',
          getReward : function(userIndex){
            var user = Rewards.users[userIndex];

            return user;
          }
        }

        $.extend(Rewards, RewardsParameters);

        // on click on col-md-3 added into the DOM
        $rewardsContainer.on("click", ".col-md-3", function(){
          // `this` is the DOM element that was clicked, it index is what we need
          var index = $(this).index();
          //userIndex we wanna edit equals the present index
          Rewards.userIndex = index;
          //edit turns true mode
          Rewards.edit = true;
          //save your reward object
          saveRewards();
          window.location.href = "reward_detail.html";

        });


        //on click on submit button addrewards
        $submitButton.on("click", function(){
          addRewards();
          saveRewards();

        });


        function addRewards(){
          //all the elements inside our clone element change here
          $rewardTitle.html($rewardTitleInput.val());
          $rewardPoint.html($rewardPointInput.val());
          $rewardDate.html($rewardDateInput.val());

          $rewardLocation.html($rewardLocationInput.val());
          $rewardImage.attr('src', $imageInputSource.attr('src'));

          Rewards.users.unshift({
            id: Rewards.users.length,
            rewardsClone : "<div class='col-md-3'> " + $rewardsClone.html() + "</div>",
            time : $rewardTimeInput.val()
          });
          var rewardData = new FormData();
          rewardData.append('reward_image_reference', $("#wizard-picture").prop('files')[0]);
          rewardData.append('title', $rewardTitleInput.val());
          rewardData.append('reward_date', $rewardDateInput.val());
          rewardData.append('location',  $rewardLocationInput.val());
          rewardData.append('reward_time', $rewardTimeInput.val());
          rewardData.append('points', $rewardPointInput.val());

          $.ajax({
            url: '/rewards',
            data: rewardData,
            contentType: false,
            cache:false,
            type: 'POST',
            processData: false,
            sucess: function (resp) {
               console.log(resp.status);
            }

          });

          window.location.href = "/rewards";
        }

        function updateRewards(){
          for (var i = 0; i < Rewards.users.length; i++) {
             $rewardsContainer.append(Rewards.users[i].rewardsClone);
          }
        }

        function saveRewards(){
          localStorage.setItem("Reward", JSON.stringify(Rewards));
        }

        function getReward(userIndex){
          var user = Rewards.users[userIndex];
          return user;
        }

        updateRewards();


    },
    productsHandler: function(){
        //the container to add our clones into....
        var $productsContainer = $("#products_container");
        //a clone of the object will be adding to container
        var $productsClone = $(".product_clone").clone(true, true);
        var $productTitle = $productsClone.find("h4.title");
        var $productPoint = $productsClone.find("span.points");
        var $productDate = $productsClone.find("span.dates");
        //var $rewardTime = $rewardsClone.find("span.time");
        var $productLocation = $productsClone.find("span.locations");
        var $productImage = $productsClone.find("img");
        var $productHistory = $productsClone.find("i.fa");
        //var $editButtoncloned = $rewardsClone.find(".editReward");
        //console.log($rewardsClone);

        //picking inputs made by users
        var $productTitleInput = $("#product_title");
        var $productPointInput = $("#product_point");
        var $productLocationInput = $("#product_location");
        var $productDateInput = $("#product_date");
        var $productTimeInput = $("#product_time");
        var $imageInputSource = $(".picture").find("img");
        var $submitButton = $("#submit_button");

        //edit tags/buttons clicked
        var $editButton = $productsContainer.find(".editProduct");

        // retrievedata from local storage if it exist
        var retrieveData = localStorage.getItem("Product");

        //old object for use incase there's is one...{}
        var oldProducts = JSON.parse(retrieveData);



        var Products = oldProducts || {};

        Products.users = Products.users || [];


        //Products Details pages logic here ......
        if (Products.users.length > 0 && Products.edit === true) {
          //get the user returned ready for editing
          var user = getProduct(Products.userIndex);
          // LET PICK ALL THE ELEMENT IN THE DETAIL PAGE
          var $products_details = $("#products_details");
          var $image = $products_details.find("img");
          var $heading = $products_details.find("h4.title");
          var $point = $products_details.find("span.points");
          var $time = $products_details.find("span.time");
          var $date = $products_details.find("span.dates");
          var $location = $products_details.find("span.locations");
          //LET PICK ALL THE VALUE FROM THE REWARD USER
          $heading.html($(user.productsClone).find("h4.title").text());
          $point.html($(user.productsClone).find("span.points").text());
          $time.html($(user.productsClone).find("span.time").text());
          $date.html($(user.productsClone).find("span.dates").text());
          $time.html(user.time);
          $location.html($(user.productsClone).find("span.locations").text());
          $image.attr("src", $(user.productsClone).find("img").attr('src'));
          //return these terms to their default state..........
          Products.edit = false;
          Products.userIndex = '';
          saveProducts();

        }

        var ProductsParameters = {
          edit: false,
          userIndex: '',
          getProduct : function(userIndex){
            var user = Products.users[userIndex];

            return user;
          }
        }

        $.extend(Products, ProductsParameters);

        // on click on col-md-3 added into the DOM
        $productsContainer.on("click", ".col-md-3", function(){
          // `this` is the DOM element that was clicked, it index is what we need
          var index = $(this).index();
          //userIndex we wanna edit equals the present index
          Products.userIndex = index;
          //edit turns true mode
          Products.edit = true;
          //save your reward object
          saveProducts();
          window.location.href = "product_detail.html";

        });


        //on click on submit button addrewards
        $submitButton.on("click", function(){
          addProducts();
          console.log(Products);
          saveProducts();

        });


        function addProducts(){
          //all the elements inside our clone element change here
          $productTitle.html($productTitleInput.val());
          $productPoint.html($productPointInput.val());
          $productDate.html($productDateInput.val());

          $productLocation.html($productLocationInput.val());
          $productImage.attr('src', $imageInputSource.attr('src'));

          Products.users.unshift({
            id: Products.users.length,
            productsClone : "<div class='col-md-3'> " + $productsClone.html() + "</div>",
            time : $productTimeInput.val()
          });
          var fdata = new FormData();
          fdata.append('product_image_reference', $('#wizard-picture').prop('files')[0]);
          fdata.append('title', $('#product_title').val());
          fdata.append('description', $productPointInput.val());
          fdata.append('quantity',  $productLocationInput.val());
          fdata.append('amount', $productTimeInput.val());

          $.ajax({
            url: '/products',
            data: fdata,
            contentType: false,
            cache:false,
            type: 'POST',
            processData: false,
            sucess: function (resp) {
                window.location.href = "/products";
            }

          });
          window.location.href = "/products";
        }

        function updateProducts(){
          for (var i = 0; i < Products.users.length; i++) {
             $productsContainer.append(Products.users[i].productsClone);
          }
        }

        function saveProducts(){
          localStorage.setItem("Product", JSON.stringify(Products));
        }

        function getProduct(userIndex){
          var user = Products.users[userIndex];
          return user;
        }

        updateProducts();


    },

    memeHandler: function(){
        var page = $(".page-notes");
        var image_file = page.find("input[type='file']");
        var background = page.find("select[name='background']");
        var font_color = page.find("select[name='font_color']");
        var text = page.find("input[type='text']");
        var position = page.find("select[name='position']");
        var imageMeme = $("#imageMeme");
        var imageText = imageMeme.find("textarea");
        var videoMeme = $("#videoMeme");
        var videoText = videoMeme.find("textarea");

        //color object..
        var colors = {
          aliceblue: 'aliceblue',
          blue : 'blue',
          green : 'green',
          blue: 'blue',
          red: 'red',
          black: '#000',
          white: '#fff',
          pink: 'pink'
        }

        //positoning
        var locations = {
          top: {
            point1 : 'top',
            value1: 0,
            point2: 'bottom',
            value2: '',
            point3: 'right',
            value3: '',
            point4: 'left',
            value4: '',
            point5: 'margin-left',
            value5: ''
          },
          bottom : {
            point1 : 'bottom',
            value1: 0,
            point2: 'top',
            value2: '',
            point3: 'right',
            value3: '',
            point4: 'left',
            value4: '',
            point5: 'margin-left',
            value5: ''
          },
          left : {
            point1 : 'left',
            value1 : 0,
            point2: 'top',
            value2: '',
            point3: 'right',
            value3: '',
            point4: 'bottom',
            value4: '',
            point5: 'margin-left',
            value5: ''
          },
          right: {
            point1 : 'right',
            value1 : 0,
            point2: 'top',
            value2: '',
            point3: 'bottom',
            value3: '',
            point4: 'left',
            value4: '',
            point5: 'margin-left',
            value5: ''
          },
          center: {
            point1 : 'left',
            value1 : '50%',
            point2 : 'margin-left',
            value2 : '-30%',
            point3: 'right',
            value3: '',
            point4: 'bottom',
            value4: '',
            point5: 'top',
            value5: ''
          }
        }

        //text inside textarea on keyup
        $(text).on('keyup', function(){
          //alert($(this).val());
          $(imageText).val($(this).val());
        });
        $(imageText).on('keyup', function(){
          //alert($(this).val());
          $(text).val($(this).val());
        });

        //change background of imagebox
        $(background).on('change', function(){
          var selected = $(this).find('option:selected').text().toLowerCase();

          if (!selected in colors) {
              return;
          }

          $(imageMeme).css({'background-color':  colors[selected] });

        });
        //change font-color of textarea
        $(font_color).on('change', function(){
          var selected = $(this).find('option:selected').text().toLowerCase();
          console.log(selected);
          if (!selected in colors) {
              return;
          }

          $(imageText).css({'color':  colors[selected] });

        });
        //change position of textarea
        $(position).on('change', function(){
          var selected = $(this).find('option:selected').text().toLowerCase();
          //console.log(locations[selected].point, locations[selected].value);
          if (!selected in locations) {
              return;
          }

         $(imageText).css(locations[selected].point1 , locations[selected].value1);
         $(imageText).css(locations[selected].point2 , locations[selected].value2);
         $(imageText).css(locations[selected].point3 , locations[selected].value3);
         $(imageText).css(locations[selected].point4 , locations[selected].value4);
         $(imageText).css(locations[selected].point5 , locations[selected].value5);

           // $(imageText).css('text-align', 'center');


        });

        //change background image
        $(image_file).change(function () {
          var file = this.files[0];
          var reader = new FileReader();
          reader.onloadend = function () {
             $(imageMeme).css('background-image', 'url("' + reader.result + '")');
          }
          if (file) {
              reader.readAsDataURL(file);
          } else {
          }
        });

    },

    memeHandler2 : function(){
        // retrievedata from local storage if it exist
        var retrieveData = localStorage.getItem("myMeme");

        //old object for use incase there's is one...{}
        var oldMemes = JSON.parse(retrieveData);
        console.log(oldMemes);

        var Meme = oldMemes || [];
        var page = $(".page-notes");
        var image_file = page.find("input[type='file']");
        var background = page.find("input[name='background']");
        var font_color = page.find("input[name='font_color']");
        var font_style = $("#font_style");
        var font_size = $("#font_size");
        var text = page.find("input[name='texts']");
        var position = page.find("select[name='position']");

        var imageMeme = $("#imageContainer");
        var previewContainer =$("#previewContainer");
        var imageText = imageMeme.find("textarea");
        var previewText = previewContainer.find("textarea");
        var memePhoto = $("#memePhoto");
        var preview = $("#preview");
        //upload button
        var upload = $("#upload");

        //display our memes
        if ($('body').data('page') == 'view_memes'){
            for (var i = 0; i < Meme.length; i++) {
                //Meme[i].photo
                $('#memes_container').append(
                    //Meme[i].photoData
                    '<div class="col-md-4">' + '<div class="thumbnail">' + '<img src=" ' + Meme[i].photoData + ' "/>' + '</div>' + '</div>'
                );
            }
        }

        //text inside textarea on keyup
        $(text).on('keyup', function(){
          $(imageText).val($(this).val());
          $(previewText).val($(this).val());
        });
        $(imageText).on('keyup', function(){
          $(text).val($(this).val());
          $(previewText).val($(this).val());
        });

        $(previewText).on('keyup', function(){
          $(text).val($(this).val());
          $(imageText).val($(this).val());
        });

        //change background color
        $(background).spectrum({
            color: "#fff",
            showPaletteOnly: true,
            palette: [
              ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
              ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
              ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
              ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
              ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
              ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
              ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
              ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
            ],
            change: function(color) {
              $(imageMeme).css("background-color" , color.toHexString());
              $(previewContainer).css("background-color" , color.toHexString());
              //hide image so background image could show
              $(memePhoto).css('display', 'none');
              $(preview).css('display', 'none');
            }
        });
        // change font-color
        $(font_color).spectrum({
            color: "#ff0",
            showPaletteOnly: true,
            palette: [
              ["#000","#444","#666","#999","#ccc","#eee","#f3f3f3","#fff"],
              ["#f00","#f90","#ff0","#0f0","#0ff","#00f","#90f","#f0f"],
              ["#f4cccc","#fce5cd","#fff2cc","#d9ead3","#d0e0e3","#cfe2f3","#d9d2e9","#ead1dc"],
              ["#ea9999","#f9cb9c","#ffe599","#b6d7a8","#a2c4c9","#9fc5e8","#b4a7d6","#d5a6bd"],
              ["#e06666","#f6b26b","#ffd966","#93c47d","#76a5af","#6fa8dc","#8e7cc3","#c27ba0"],
              ["#c00","#e69138","#f1c232","#6aa84f","#45818e","#3d85c6","#674ea7","#a64d79"],
              ["#900","#b45f06","#bf9000","#38761d","#134f5c","#0b5394","#351c75","#741b47"],
              ["#600","#783f04","#7f6000","#274e13","#0c343d","#073763","#20124d","#4c1130"]
            ],
            change: function(color) {
                $(imageText).css("color" , color.toHexString());
                $(previewText).css("color" , color.toHexString());
            }
        });

        //font-style note for dropdown btn-group
        $('.fonts li > a').click(function(e){
          $(font_style).text(this.innerHTML);
          $(imageText).css('font-family', " " + this.innerHTML);
          $(previewText).css('font-family', " " + this.innerHTML);
        });

         //font-size note for dropdown btn-group
        $('.sizes li > a').click(function(e){
          $(font_size).text(this.innerHTML);
          $(imageText).css('font-size', this.innerHTML + 'px');
          $(previewText).css('font-size', (this.innerHTML/1.5) + 'px');
        });

        //general btn group change
        $(".btn-group button").on('click', function(){
          //alert($(this).attr('name'));
          switch($(this).attr('name')) {
            case 'bold':
                $(imageText).css('font-weight') == '100' ? $(imageText).css('font-weight', '900') : $(imageText).css('font-weight', '100');
                $(previewText).css('font-weight') == '100' ? $(previewText).css('font-weight', '900') : $(previewText).css('font-weight', '100');
                break;
            case 'italics':
                $(imageText).css('font-style') == 'normal' ? $(imageText).css('font-style', 'italic') : $(imageText).css('font-style', 'normal');
                $(previewText).css('font-style') == 'normal' ? $(previewText).css('font-style', 'italic') : $(previewText).css('font-style', 'normal');
                break;
            case 'strikethrough':
                $(imageText).css('text-decoration') == 'none' ? $(imageText).css('text-decoration', 'line-through') : $(imageText).css('text-decoration', 'none');
                $(previewText).css('text-decoration') == 'none' ? $(previewText).css('text-decoration', 'line-through') : $(previewText).css('text-decoration', 'none');
                break;
            case 'underline':
                $(imageText).css('text-decoration') == 'none' ? $(imageText).css('text-decoration', 'underline') : $(imageText).css('text-decoration', 'none');
                $(previewText).css('text-decoration') == 'none' ? $(previewText).css('text-decoration', 'underline') : $(previewText).css('text-decoration', 'none');
                break;
            case 'center':
              $(imageText).css('text-align', 'center');
              $(previewText).css('text-align', 'center');
              break;
            case 'left':
              $(imageText).css('text-align', 'left');
              $(previewText).css('text-align', 'left');
              break;
            case 'right':
              $(imageText).css('text-align', 'right');
              $(previewText).css('text-align', 'right');
              break;
            case 'middle':
              $(imageText).css({'top' : '50%', 'margin-top' : '-5%'});
              $(previewText).css({'top' : '50%', 'margin-top' : '-5%'});
              break;
            case 'top':
              $(imageText).css({'top' : '0px', 'margin-top' : '0', 'text-align' : 'left', 'bottom' : ''});
              $(previewText).css({'top' : '0px', 'margin-top' : '0', 'text-align' : 'left', 'bottom' : ''});
              break;
            case 'bottom':
              $(imageText).css({'top' : '', 'margin-top' : '0', 'text-align' : 'left', 'bottom' : '0px'});
              $(previewText).css({'top' : '', 'margin-top' : '0', 'text-align' : 'left', 'bottom' : '0px'});
              break;
          }
        });


        //change background image
        $(upload).on('click', function(){
          $(image_file).click();
        });

        $(image_file).change(function () {

          var file = this.files[0];
          var reader = new FileReader();
          reader.onloadend = function () {
             //$(imageMeme).css('background-image', 'url("' + reader.result + '")');
             $(memePhoto).attr('src', reader.result);
             $(preview).attr('src', reader.result);
             //show photo when file change
             $(memePhoto).css('display', 'initial');
             $(preview).css('display', 'initial');
          }
          if (file) {
              reader.readAsDataURL(file);
          } else {
          }
        });


        //$(memePhoto).dragncrop();
        $('#preview').dragncrop({
          drag: function(event, position){
            $(memePhoto).dragncrop('move', position);
          }
        });
        $(memePhoto).dragncrop({
            centered: true,
            overflow: false,
            drag: function(event, position){
              $('#preview').dragncrop('move', position);
            }
        });

        //encode preview section
        $("#saveMeme").on("click", function(){
            //var data = JSON.stringify($(previewContainer).clone(true, true));
            html2canvas($(previewContainer), {
                onrendered: function(canvas) {
                    var data = canvas.toDataURL("image/png", 1.0);
                    var image = new Image();
                    image.src = data;
                    Meme.unshift({
                        id: Meme.length,
                        photoData: data,
                        photo: image
                    });
                    // console.log(image);
                    // localStorage.setItem("myMeme", JSON.stringify(Meme));
                    $('#hidden_image').val(data);

                    var memeData = new FormData(document.forms["postImage"]);
                    // memeData.append('image_reference', $('#hidden_image').val());
                    console.log(memeData.get('image_reference'));


                    $.ajax({
                      url: '/memes',
                      data: memeData,
                      contentType: false,
                      cache:false,
                      type: 'POST',
                      processData: false,
                      sucess: function (resp) {
                          window.location.href = "/memes";
                      }
                    });
                    window.location.href = "/memes";
                    //$('#img-out').append(image);
                    //document.body.removeChild(canvas);
                }
            });
            //console.log(data);

        });



      },

      dateRanger: function(){
        $('#reportrange').daterangepicker( {
              ranges: {
                 'Today': [moment(), moment()],
                 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
                 'Last 7 Days': [moment().subtract('days', 6), moment()],
                 'Last 30 Days': [moment().subtract('days', 29), moment()],
                 'This Month': [moment().startOf('month'), moment().endOf('month')],
                 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
              },
              startDate: moment().subtract('days', 29),
              endDate: moment()
            },
            function(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
        );
        },
      counter: function(){
        $('.widget-thumb-body-stat').each(function() {
          var $this = $(this),
              countTo = $this.attr('data-value');

          $({ countNum: $this.text()}).animate({
            countNum: countTo
          },

          {

            duration: 5000,
            easing:'linear',
            step: function() {
              $this.text(Math.floor(this.countNum));
            },
            complete: function() {
              $this.text(this.countNum);
              //alert('finished');
            }

          });
        });
      }

    }


})();