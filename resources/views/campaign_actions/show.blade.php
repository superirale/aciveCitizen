@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">campaign_action {{ $campaign_action->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('campaign_actions/' . $campaign_action->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit campaign_action"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['campaign_actions', $campaign_action->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete campaign_action',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $campaign_action->id }}</td>
                                    </tr>
                                    <tr><th> Campaign Id </th><td> {{ $campaign_action->campaign_id }} </td></tr><tr><th> Action Id </th><td> {{ $campaign_action->action_id }} </td></tr><tr><th> Message </th><td> {{ $campaign_action->message }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection