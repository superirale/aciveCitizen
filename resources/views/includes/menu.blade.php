<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#sidebar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="index.html"></a>-->
                <div class="navbar-brand">
                    <a href="index.html" id="logo">
                        <img src="/dashboard/assets/img/logo_active.png" alt="logo" style="width: 100%;">
                    </a>
                    <div class="menu-toggler sidebar-toggler">
                         <a id="menu-medium" class="sidebar-toggle tooltips">
                            <i class="fa fa-outdent"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="navbar-center">
                <i id="search-link" class="fa fa-search"></i>
                <input id="search-area" class="form-control" placeholder="Search for something" type="search">
            </div>
            <div class="navbar-collapse collapse">
                <!-- BEGIN TOP NAVIGATION MENU -->
                <ul class="nav navbar-nav pull-right header-menu">

                    <!-- BEGIN USER DROPDOWN -->
                    <li class="dropdown" id="user-header">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img src="/dashboard/assets/img/avatar2.png" alt="user avatar" width="30" class="p-r-5">
                            <span class="username">{{Auth::user()->name}}</span>
                            <i class="fa fa-angle-down p-r-10"></i>
                        </a>
                        <ul class="dropdown-menu">

                            <li>
                                <a href="/admin/settingd">
                                    <i class="glyph-icon flaticon-settings21"></i> Account Settings
                                </a>
                            </li>
                            <li>
                                <a href="/logout"><i class="fa fa-power-off"></i> Logout </a>
                            </li>
                            {{-- <li class="dropdown-footer clearfix">
                            <a href="javascript:;" class="toggle_fullscreen" title="Fullscreen">
                                <i class="glyph-icon flaticon-fullscreen3"></i>
                            </a>
                            <a href="lockscreen.html" title="Lock Screen">
                                <i class="glyph-icon flaticon-padlock23"></i>
                            </a>
                            <a href="login.html" title="Logout">
                                <i class="fa fa-power-off"></i>
                            </a>
                        </li> --}}
                        </ul>
                    </li>
                    <!-- END USER DROPDOWN -->
                </ul>
                <!-- END TOP NAVIGATION MENU -->
            </div>
        </div>
    </nav>