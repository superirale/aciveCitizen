@if(Auth::check())

<!--=========================================================================
				HEADER AND ALL IT CONTENTS NAVBAR, SEARCH BAR BEGIN HERE
			========================================================================-->
			<header class="clearfix">
				<!--nav bar enters this place -->
				<nav class="navbar navbar-inverse " role="navigation">
			        <div class="container-fluid">
			            <div class="navbar-header">
			            	<!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
			                    <span class="sr-only">Toggle navigation</span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
		                	</button>-->
		                	<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
		                        <span class="hamb-top"></span>
		                        <span class="hamb-middle"></span>
		                        <span class="hamb-bottom"></span>
	                    	</button>
			                <!--<a class="navbar-brand" href="index.html">&#83;&#65;&#77;&#83;&#79;&#78;</a>-->
			            </div>
			            <div class="navbar-center">
			            	<div class="logo-wrapper">
			            		<a href="/home">
				            		<div class="thumbnail">
				            			<img src="users/img/logo.png">
				            		</div>
			            		</a>
			            	</div>
			            </div>
			            <div class="navbar-collapse collapse">
			                <!-- BEGIN TOP NAVIGATION MENU -->
			                <ul class="nav navbar-nav pull-right header-menu">
			                	<!--Button outline type-->
			                	<li class="dropdown">
			                        <a href="#" id="buttonboxback"  data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			                            Create Campaign
			                        </a>
			                    </li>
			                	<!-- BEGIN SEARCH DROPDOWN -->
			                    <li class="dropdown" id="messages-header">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" id="search-link">
			                            <i class="fa fa-search"></i>
			                        </a>
			                    </li>
			                    <!-- END SEARCH DROPDOWN -->
			                    <!-- this calls sign in modal box -->
			                   <!-- <li class="dropdown-toggle">
			                        <a href="#" data-toggle="modal" data-target="#signInModal">
			                            Sign In
			                        </a>
			                    </li>-->
			                    <!-- this calls sign up modal box -->
			                   <!-- <li class="dropdown-toggle">
			                        <a href="#" data-toggle="modal" data-target="#signUpModal">
			                            Sign Up
			                        </a>
			                    </li>-->
			                    <!-- BEGIN NOTIFICATION DROPDOWN -->
			                    <li class="dropdown" id="notifications-header">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			                            <i class="fa fa-bell fa"></i>
			                        </a>
			                        <ul class="dropdown-menu">
			                            <li class="dropdown-header clearfix">
			                                <p class="pull-left">Notifications</p>
			                            </li>
			                            <li>
			                                <ul class="dropdown-menu-list withScroll" data-height="220">
			                                    <li>
			                                        <a href="#">
			                                            <i class="fa fa-birthday-cake"></i>
			                                            something new
			                                            <span class="dropdown-time">Just now</span>
			                                        </a>
			                                    </li>
			                                    <li>
			                                        <a href="#">
			                                            <i class="fa fa-file-text p-r-10 f-18"></i>
			                                            Something new again
			                                            <span class="dropdown-time">22 mins</span>
			                                        </a>
			                                    </li>
			                                    <li>
			                                        <a href="#">
			                                            <i class="fa fa-picture-o p-r-10 f-18 c-blue"></i>
			                                            Another new one
			                                            <span class="dropdown-time">2 days</span>
			                                        </a>
			                                    </li>
			                                </ul>
			                            </li>
			                            <li class="dropdown-footer clearfix">
			                                <a href="#" class="pull-left">See all notifications</a>
			                                <a href="#" class="pull-right">
			                                    <i class="fa fa-cog"></i>
			                                </a>
			                            </li>
			                        </ul>
			                    </li>
			                    <!-- END NOTIFICATION DROPDOWN -->
			                    <!-- BEGIN USER DROPDOWN -->
			                    <li class="dropdown" id="user-header">
			                        <a href="#" class="dropdown-toggle c-white" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			                            <i class="fa fa-user"></i>
			                            <span class="username">{{Auth::user()->name}}</span>
			                            <i class="fa fa-angle-down p-r-10"></i>
			                        </a>
			                        <ul class="dropdown-menu">
			                            <li>
			                                <a href="/profile">
			                                    <i class="fa fa-user"></i> My Profile
			                                </a>
			                            </li>
			                            <li>
			                                <a href="/profile">
			                                    <i class="fa fa-life-ring"></i> My Points
			                                </a>
			                            </li>
			                            <li>
			                                <a href="/settings">
			                                    <i class="fa fa-cog"></i> Account Settings
			                                </a>
			                            </li>
			                            <li>
			                                <a href="/friend">
			                                    <i class="fa fa-male"></i> Refer Friend
			                                </a>
			                            </li>
			                            <li class="dropdown-footer clearfix">
										<a href="javascript:;" class="toggle_fullscreen" title="Fullscreen">
											<i class="glyph-icon flaticon-fullscreen3"></i>
										</a>
										<a href="lockscreen.html" title="Lock Screen">
											<i class="glyph-icon flaticon-padlock23"></i>
										</a>
										<a href="{{ url('/logout') }}" title="Logout" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
											<i class="fa fa-power-off"></i>
										</a>
										  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
									</li>
			                        </ul>
			                    </li>
			                    <!-- END USER DROPDOWN -->

			                    <!-- BEGIN CHAT HEADER -->
			                   
			                    <!-- END CHAT HEADER -->
			                </ul>
			                <!-- END TOP NAVIGATION MENU -->
			            </div>
			        </div>
			    </nav>
				<!-- nav bar ends here -->
				
				<div class="action-lists col-md-12">
					<!--search box like the alert box-->
					<div class="search-area col-md-12" id="search-area">
						<form>
							<input type="text" name="search" placeholder="Search.."/>
							<button type="submit" style="display: inline-block; width: 46px; background: none; border: none; padding: 0;text-align: left;margin: 16px 0 0 0;">
								<span class="fa fa-search fa-3x"></span>
							</button>
						</form>
					</div>
					<ul>
						<li class="col-md-3 col-sm-3"><a href="/campaigns">Campaigns</a></li>
						<!--<li class="col-md-3 col-sm-3"><a href="#">Programmes</a></li>-->
						<li class="col-md-3 col-sm-3"><a href="/rewards">Rewards</a></li>
						<li class="col-md-3 col-sm-3"><a href="/store">Store</a></li>
						<li class="col-md-3 col-sm-3"><a href="/blog">Blog</a></li>
					</ul>
				</div>
				<div class="col-md-12 alert-clone">
					<div class="alert-content">
						<p>Active Citizens is a campaign platform for activists, advocates, civil societies and other categories of active citizens.</p>
					</div>
				</div>
				<!-- Sidebar -->
		        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
		            <ul class="nav sidebar-nav">
		            	<li class="sidebar-brand">
                        	<a href="/home">Home</a>
                        </li>
		                <li>
		                    <a href="/Campaigns">Campaign</a>
		                </li>
		                <li>
		                	<a href="/campaignreward">Rewards</a>
		                </li>
		                 <li>
		                    <a href="/profile">Profile</a>
		                </li>
		                 <li>
		                    <a href="/setting">Account Settings</a>
		                </li>
		                <li>
		                    <a href="/friend">Refer a Friend</a>
		                </li>
		            </ul>
		        </nav>
	            <!-- /#sidebar-wrapper -->
			</header>

@else
<header class="clearfix">
				<!--nav bar enters this place -->
				<nav class="navbar navbar-inverse " role="navigation">
			        <div class="container-fluid">
			            <div class="navbar-header">
			            	<!--<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar">
			                    <span class="sr-only">Toggle navigation</span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
			                    <span class="icon-bar"></span>
		                	</button>-->
		                	<button type="button" class="hamburger is-closed" data-toggle="offcanvas">
		                        <span class="hamb-top"></span>
		                        <span class="hamb-middle"></span>
		                        <span class="hamb-bottom"></span>
	                    	</button>
			                <!--<a class="navbar-brand" href="index.html">&#83;&#65;&#77;&#83;&#79;&#78;</a>-->
			            </div>
			            <div class="navbar-center">
			            	<div class="logo-wrapper">
			            		<a href="/">
				            		<div class="thumbnail">
				            			<img src="/users/img/logo.png">
				            		</div>
			            		</a>
			            	</div>
			            </div>
			            <div class="navbar-collapse collapse">
			                <!-- BEGIN TOP NAVIGATION MENU -->
			                <ul class="nav navbar-nav pull-right header-menu">
			                	<!--Button outline type-->
			                	<li class="dropdown">
			                        <a href="#" id="buttonboxback"  data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			                            Create Campaign
			                        </a>
			                    </li>
			                	<!-- BEGIN SEARCH DROPDOWN -->
			                    <li class="dropdown" id="messages-header">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" id="search-link">
			                            <i class="fa fa-search"></i>
			                        </a>
			                    </li>
			                    <!-- END SEARCH DROPDOWN -->
			                    <!-- this calls sign in modal box -->
			                    <li class="dropdown-toggle">
			                        <a href="#" data-toggle="modal" data-target="#signInModal">
			                            Sign In
			                        </a>
			                    </li>
			                    <!-- this calls sign up modal box -->
			                    <li class="dropdown-toggle">
			                        <a href="#" data-toggle="modal" data-target="#signUpModal">
			                            Sign Up
			                        </a>
			                    </li>
			                    <!-- BEGIN NOTIFICATION DROPDOWN -->
			                    <!--<li class="dropdown" id="notifications-header">
			                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			                            <i class="fa fa-bell fa"></i>
			                        </a>
			                        <ul class="dropdown-menu">
			                            <li class="dropdown-header clearfix">
			                                <p class="pull-left">Notifications</p>
			                            </li>
			                            <li>
			                                <ul class="dropdown-menu-list withScroll" data-height="220">
			                                    <li>
			                                        <a href="#">
			                                            <i class="fa fa-birthday-cake"></i>
			                                            something new
			                                            <span class="dropdown-time">Just now</span>
			                                        </a>
			                                    </li>
			                                    <li>
			                                        <a href="#">
			                                            <i class="fa fa-file-text p-r-10 f-18"></i>
			                                            Something new again
			                                            <span class="dropdown-time">22 mins</span>
			                                        </a>
			                                    </li>
			                                    <li>
			                                        <a href="#">
			                                            <i class="fa fa-picture-o p-r-10 f-18 c-blue"></i>
			                                            Another new one
			                                            <span class="dropdown-time">2 days</span>
			                                        </a>
			                                    </li>
			                                </ul>
			                            </li>
			                            <li class="dropdown-footer clearfix">
			                                <a href="#" class="pull-left">See all notifications</a>
			                                <a href="#" class="pull-right">
			                                    <i class="fa fa-cog"></i>
			                                </a>
			                            </li>
			                        </ul>
			                    </li>-->
			                    <!-- END NOTIFICATION DROPDOWN -->
			                    <!-- BEGIN USER DROPDOWN -->
			                    <!--<li class="dropdown" id="user-header">
			                        <a href="#" class="dropdown-toggle c-white" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			                            <i class="fa fa-user"></i>
			                            <span class="username">Joseph</span>
			                            <i class="fa fa-angle-down p-r-10"></i>
			                        </a>
			                        <ul class="dropdown-menu">
			                            <li>
			                                <a href="profile.html">
			                                    <i class="fa fa-user"></i> My Profile
			                                </a>
			                            </li>
			                            <li>
			                                <a href="profile.html">
			                                    <i class="fa fa-life-ring"></i> My Points
			                                </a>
			                            </li>
			                            <li>
			                                <a href="setting.html">
			                                    <i class="fa fa-cog"></i> Account Settings
			                                </a>
			                            </li>
			                            <li>
			                                <a href="friend.html">
			                                    <i class="fa fa-male"></i> Refer Friend
			                                </a>
			                            </li>
			                            <li class="dropdown-footer clearfix">
										<a href="javascript:;" class="toggle_fullscreen" title="Fullscreen">
											<i class="glyph-icon flaticon-fullscreen3"></i>
										</a>
										<a href="lockscreen.html" title="Lock Screen">
											<i class="glyph-icon flaticon-padlock23"></i>
										</a>
										<a href="index.html" title="Logout">
											<i class="fa fa-power-off"></i>
										</a>
									</li>
			                        </ul>
			                    </li>-->
			                    <!-- END USER DROPDOWN -->

			                    <!-- BEGIN CHAT HEADER -->
			                   
			                    <!-- END CHAT HEADER -->
			                </ul>
			                <!-- END TOP NAVIGATION MENU -->
			            </div>
			        </div>
			    </nav>
				<!-- nav bar ends here -->
				
				<div class="action-lists col-md-12">
					<!--search box like the alert box-->
					<div class="search-area col-md-12" id="search-area">
						<form>
							<input type="text" name="search" placeholder="Search.."/>
							<button type="submit" style="display: inline-block; width: 46px; background: none; border: none; padding: 0;text-align: left;margin: 16px 0 0 0;">
								<span class="fa fa-search fa-3x"></span>
							</button>
						</form>
					</div>
					<ul>
						<li class="col-md-3 col-sm-3"><a href="/viewcampaigns">Campaigns</a></li>
						<!--<li class="col-md-3 col-sm-3"><a href="#">Programmes</a></li>-->
						<li class="col-md-3 col-sm-3"><a href="/reward">Rewards</a></li>
						<li class="col-md-3 col-sm-3"><a href="/store">Store</a></li>
						<li class="col-md-3 col-sm-3"><a href="/blog">Blog</a></li>
					</ul>
				</div>
				<div class="col-md-12 alert-clone">
					<div class="alert-content">
						<p>Active Citizens is a campaign platform for activists, advocates, civil societies and other categories of active citizens.</p>
					</div>
				</div>
				<!-- Sidebar -->
		        <nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
		            <ul class="nav sidebar-nav">
		            	<li class="sidebar-brand">
                        	<a href="/">Home</a>
                        </li>
		                <li>
		                    <a href="/Campaigns">Campaign</a>
		                </li>
		                <li>
		                	<a href="/rewards">Rewards</a>
		                </li>
		                 <li>
		                    <a href="/profile">Profile</a>
		                </li>
		                 <li>
		                    <a href="/setting">Account Settings</a>
		                </li>
		                <li>
		                    <a href="/friend">Refer a Friend</a>
		                </li>
		            </ul>
		        </nav>
	            <!-- /#sidebar-wrapper -->
			</header>


@endif

