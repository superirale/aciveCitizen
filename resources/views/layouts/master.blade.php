
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
<!DOCTYPE html>
<html lang="en"  class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Active Citizen</title>

        <!-- Font Awesome for awesome icons. You can redefine icons used in a plugin configuration -->
        <link href="/users/bower_components/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet">
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="/users/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!--swiper css here-->
        <link rel="stylesheet" type="text/css" href="/users/css/swiper.min.css">
        <!--Main css goes here -->
        <link rel="stylesheet" type="text/css" href="/users/css/main.css">
        <!--responsive css -->
        <link rel="stylesheet" type="text/css" href="/users/css/responsive.css">
    
    </head>

  <body data="
  home">
  <div class="se-pre-con"></div>
  <div id="wrapper">
  @include('layouts.nav')

    

    


    	@yield('content')
    	

    	
    

     



   @include('layouts.footer')
   </div>


   <!-- JS -->
        <script src="/users/bower_components/jquery-3.1.0.min/index.js"></script>
        <script src="/users/bower_components/underscore-min.js"></script>
        <script src="/users/bower_components/sanitize.js"></script>
        <!-- Bootstrap JavaScript -->
        <script src="/users/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="/users/bower_components/modernizr.js"></script>
        <!-- Swiper JS -->
        <script src="/users/bower_components/swiper.min.js"></script>
        <script src="/users/js/main.js"></script>
        <script src="/users/js/swiperController.js"></script>
        <script src="/users/js/sidebar.js"></script>
        <script src="/users/js/validationController.js"></script>
        <script type="text/javascript">
            $(window).on('load', function() { 
                $(".se-pre-con").fadeOut(5500);
            });
        </script>
  </body>
</html>
