<footer>
				<div class="col-md-12 partners-container">
							<h3 class="partners-header">Partners</h3>
							<div class="col-md-12 gallery-container"> 
								
							    <div class="swiper-container swiper-contained">
							        <div class="swiper-wrapper">
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							            <div class="swiper-slide">
							            	<div class="thumbnail">
							            		<img src="/users/img/logo.png">
							            	</div>
							            </div>
							        </div>
							        
							        <div class="swiper-pagination"></div>
							    </div>
							</div>
						</div>
				<div class="container">
					<div class="row">
						<div class="col-md-3">
							<a href="#">
							<div class="thumbnail">
								<img src="/users/img/logo.png">
							</div>
							</a>
						</div>
						<div class="col-md-6">
							<h4>About Us</h4>
							<ul class="inline">
								<li>
									<a href="#">Who We Are</a>
								</li>
								<li class="pipe">|</li>
								<li>
									<a href="#">Contact Us</a>
								</li>
								<li class="pipe">|</li>
								<li>
									<a href="#">Feedback</a>
								</li>
							</ul>
						</div>
						<div class="col-md-3">
							<h4>Get Involve</h4>
							<ul class="inline">
								<li>
									<a href="#">
										<i class="fa fa-facebook fa-2x"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-twitter fa-2x"></i>
									</a>
								</li>
								<li>
									<a href="#">
										<i class="fa fa-instagram fa-2x"></i>
									</a>
								</li>
							</ul>
						</div>
						<div class="col-md-12">
							<ul class="inline">
								<li>
									<a href="#">Privacy</a>
								</li>
								<li class="pipe">|</li>
								<li>
									<a href="#">Terms and Conditions</a>
								</li>
							</ul>
							<p>&copy; 2016 Project 321 All right reserved</p>
						</div>
					</div>
				</div>
			</footer>