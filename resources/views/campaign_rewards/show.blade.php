@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">campaign_reward {{ $campaign_reward->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('campaign_rewards/' . $campaign_reward->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit campaign_reward"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['campaign_rewards', $campaign_reward->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete campaign_reward',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $campaign_reward->id }}</td>
                                    </tr>
                                    <tr><th> Campaign Id </th><td> {{ $campaign_reward->campaign_id }} </td></tr><tr><th> Reward Id </th><td> {{ $campaign_reward->reward_id }} </td></tr><tr><th> Deleted At </th><td> {{ $campaign_reward->deleted_at }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection