@extends('layouts.master')
@section('content')


<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGNS ETC BAR BEGIN HERE
			========================================================================-->
			<section class="content-area clearfix">
				<div class="sections">
					<div class="container">
						
						<a href="#" id="action-category">food and hunger</a>
				
						<div class="row">
							<div class="col-md-6">
								<p>
									<span id="action-header">
										Join the Movement to Tackle Hunger
									</span>
									<span class="social-medias">
										<a href="#">
											<i class="fa fa-facebook"></i>
										</a>
										<a href="#">
											<i class="fa fa-twitter"></i>
										</a>
										<a>
											<i class="fa fa-google-plus"></i>
										</a>
									</span>
								</p>
								<div class="thumbnail">
									<img src="/users/img/example.jpg">
								</div>
								<p>Nearly 795 million people around the world are hungry, and yet there is enough food in the world to feed everyone. It’s time to address this inequity once and for all.

								In advance of the G7 next year, Global Citizens are calling on world leaders to make financial commitments that will help encourage other G7 countries to make political and financial commitments at next year's summit.

								Italy has been a leader in food and nutrition policy. Next year, when Italy chairs the G7, the Italian government will have an opportunity to set a precedent for global leaders to prioritize the food and nutrition agenda in order to end hunger for good.

								In advance of the G7 next year, Global Citizens are calling on the Italian government to make a financial commitment that will help encourage other G7 countries to make political and financial commitments at next year's summit.

								Place a phone call to the Italian Embassy in Washington, D.C and ask them to make ending hunger a priority at the G7 next year by announcing new funding to lift 500 million people out of hunger and malnutrition.
								</p>
							</div>
							<div class="col-md-6">
								<h4>Take Action</h4>
								<ol>
									<li>Add your Phone number below
								          <div class="input-group">
									            <input type="email" class="form-control" placeholder="Phone Number" required id="mc-email">
									            <span class="input-group-btn">
									            	<button type="submit" class="btn btn-custom">SUBMIT <i class="fa fa-envelope"></i> 
									            	</button>
									            </span> 
								            </div>
								          <label for="mc-email" id="mc-notification"></label>
									</li>
									<li>We'll call you and connect you with the target (at no charge)</li>
									<li>Call Italy and urge them to prioritize hunger at the G7 next year.
										<div class="actions">
											<h6>Tweet</h6>
											<div class="tweet-texts">
												@fhollande @ValliniAndre #YouToldRihanna education is France’s #1 priority- so will you commit generously to #EducationCannotWait?
											</div>
											<p><span class="action-points">3 points</span> to earn</p>
											<a href="" class="btn btn-twitter"><i class="fa fa-twitter"></i>Send a Tweet</a>
										</div>
									</li>
									<li>
										<div class="actions">
											<h6>Instagram</h6>
											<div class="instagram-texts">
												Instagram a photo of yourself with the book that changed your life using the hashtag #InTimesOfCrisis and tell World Leaders: support education. Keep in mind that we can’t see your post if your account is private.
											</div>
											<p><span class="action-points">3 points</span> to earn</p>
											<a href="" class="btn btn-instagram"><i class="fa fa-instagram"></i>Complete Action</a>
										</div>
									</li>
									<li>
										<div class="actions">
											<h6>Email</h6>
											<div class="email-texts">
												Send an email urging world leaders to prioritize support for sanitation initiatives. <a href="#" class="inline">Read More</a>
											</div>
											<p><span class="action-points">3 points</span> to earn</p>
											<a href="" class="btn btn-email"><i class="fa fa-envelope"></i>Complete Action</a>
										</div>
									</li>
								</ol>
								<!--<div style="text-align: center;">
									<a type="button" class="btn btn-custom" style="min-width: 163px; font-size: 18px; padding-top: 12px; padding-bottom: 12px;">Make a call</a>
								</div>-->
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGN ETC BAR END HERE
			========================================================================-->
@endsection