@extends('layouts.master')

@section('content')
	<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGNS ETC BAR BEGIN HERE
			========================================================================-->
			<section class="content-area clearfix">
				<div class="container friends-content">
					<h1 class="heading">Get your friends to join the movement.</h1>
					<div class="row">
						<div class="col-md-6">
							<h3 class="user-heading">How it works</h3>
							<p>
								Our mission is to build the largest possible movement of global citizens taking action to solve the world's biggest challenges.<br/><br/>

								Earn 5 points for every five friends, who register to become Global Citizens with your link.<br/><br/>

								When your friends sign up, their names will appear in the "Recent recruit" section on this page. If you have any issues please get in touch with us at <a href="#">help@activecitizen.org.</a>
							</p>
							<div class="iframe-container">
								<iframe style="width: 100%; height: 400px;" src="https://www.youtube.com/embed/rC9TnpK0z6E" frameborder="0" allowfullscreen></iframe>
							</div>
							
						</div>
						<div class="col-md-6">
							
							<div class="col-md-12" id="linkContainer">
								<h3 class="user-heading">Share your invite link</h3>
								<div class="col-md-8">
									<input type="text" class="form-control" value="glblctzn.me/2dFA5Kt" id="copyTarget" readonly>
								</div>
								<div class="col-md-4">
									<a href="#" class="btn btn-outline" id="copyButton">copy link</a>
								</div>
							</div>
							<div class="col-md-12" id="buttonContainer">
								<div class="col-md-6 col-xs-12">
									<a href="#" class="btn btn-block btn-facebook">
										<i class="fa fa-facebook"></i> Facebook
									</a>
								</div>
								<div class="col-md-6 col-xs-12">
									<a href="#" class="btn btn-block btn-twitter">
										<i class="fa fa-twitter"></i> Twitter
									</a>
								</div>
								<div class="col-md-6 col-xs-12">
									<a href="#" class="btn btn-block btn-google">
										<i class="fa fa-instagram"></i> Instagram
									</a>
								</div>
								<div class="col-md-6 col-xs-12">
									<a href="#" class="btn btn-block btn-email">
										<i class="fa fa-envelope"></i> Email
									</a>
								</div>
							</div>
							<h3 class="user-heading">Recent recruits</h3>
							<p>Hey, we‘ve noticed your friends haven’t signed up yet.</p>
						</div>
					</div>
				</div>
			</section>
			<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGN ETC BAR END HERE
			========================================================================-->

@endsection