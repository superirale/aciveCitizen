@extends('layouts.master')
@section('content')
	<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGNS ETC BAR BEGIN HERE
			========================================================================-->
			<section class="content-area clearfix">
				<div class="reward-banner" style="background-image: url('/users/img/banner.jpg');">
					<div class="reward-content">
						<div class="reward-view">
							<h1 class="reward-global-info">Ben Harper &amp; The Innocent Criminals </h1>
							<p class="reward-global-action">   
			                    Redeem your points to win tickets to see Ben Harper &amp; The Innocent Criminals ! Or earn more points by taking action <a href="#" class="inline">here.</a>  
			                </p>
			                <div class="article_actions">
			                	<p>
			                        Follow Ben Harper &amp; The Innocent Criminals 
			                    </p>
			                	<div class="article_actions__inner">
				                	<a class="web-share-icon social-share tip" href="#" target="_blank">
				                		<span class="fa fa-link fa-2x"></span>
				                	</a>
				                	<a class="facebook social-share tip" title="Share on Facebook" href="#" target="_blank">
				                		<span class="fa fa-facebook hidden-md fa-2x">&nbsp;</span>
				                	</a>
				                	<a class="social-share twitter" title="Share on Twitter" href="#" target="_blank">
				                		<span class="fa fa-twitter hidden-md fa-2x"></span>
				                	</a>
			                	</div>
			                </div>
						</div>
					</div>
				</div>
				<div class="container" id="gallery-list">
					<div class="row">
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container is-flex">
								<div class="col-md-6 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-description">
											<h3>
												2 tickets to Lissie in Philadelphia on October 10, 2016.
											</h3>
											<h5 class="reward-date">
												<span class="reward-detail__icon"><i class="fa fa-calendar-o"></i></span>
												<span class="reward-d-description">
	                                        		Oct. 11, 2016
	                                    		</span>
	                                    	</h5>
	                                    	<h5 class="reward-date">
												<span class="reward-detail__icon"><i class="fa fa-clock-o"></i></span>
												<span class="reward-d-description">
	                                        		7pm
	                                    		</span>
	                                    	</h5>
											<div class="story-reward">
												<a href="#" class="btn btn-normal btn-black">Enter to win</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2 col-xs-12 reward-border">
									<div class="col-md-12 reward-gallery" id="circle-container">
										<div class="reward-circle">
											<span class="reward-detail-stats__num">
	                                            2
	                                        </span>
	                                        <span class="reward-detail-stats__description">
	                                            tickets per winner
	                                        </span>
										</div>
										<div class="reward-circle">
											<span class="reward-detail-stats__num">
	                                            2
	                                        </span>
	                                        <span class="reward-detail-stats__description">
	                                            tickets per winner
	                                        </span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGN ETC BAR END HERE
			========================================================================-->





@endsection