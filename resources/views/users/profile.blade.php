@extends('layouts.master')

@section('content')
	<section class="content-area clearfix">
				<div class="jumbotron" style="background-image: url(/users/img/books.jpg);">
					<div class="user-timeline-container">
				    	<div class="timeline-info">
				    		<div class="avatar_holder">
				    			<h5>{{Auth::user()->name}}</h5>
				    			<img class="affiliate_logo" src="{{Auth::user()->avatar}}"> 
				    			<!--src="img/avatar.png"-->
				    		</div>
				    		<div class="affiliate_content_info">
					    		<div class="affiliate_content_info-text-top">
					    			<p></p>
					    		</div>
					    		<div class="affiliate_content_info-points">
						    		<div class="points-available">
							    		<span>
								          Available Points
								        </span>
								        <h4>1</h4>
							        </div>
							        <div class="points-earned">
								        <span>
								          Lifetime Earned Points
								        </span>
								        <h4>1</h4>
							        </div>
						        </div>
					        </div>
				    	</div>
			    	</div>
				</div>
				<div class="container">
					<div class="row">
						<!--<div class="col-md-8">
							<div class="feed-detail">
								<div class="most-recent-updates">
									<h4 class="section-title">Most Recent<strong> Updates</strong>
									</h4>
									<div class="most-recent-updates-wrapper">
										<div id="user-feed-container">
											<div class="empty-feed-placeholder">Start following issues or campaigns that are important to you and related information will appear here!
											</div>
										</div>
									</div>
								</div>
								<div class="aside-content">
									<div class="aside-content-follow-issues">
										
									</div>
								</div>
							</div>
						</div>-->
						<div class="col-md-12">
							<div class="section-head section-head-padder">
								<h5>Your Recent Actions</h5>
							</div>
							<!--recent action list to be repeated as action increases begins-->
							<div class="recent-action-container">
								<div class="col-md-3">
									<div class="points-per-action">
										<p><span>Welcome to Active Citizen</span></p>
									</div>
								</div>
								<div class="col-md-6">
									<ol>
										<li class="article-recent">
											<div class="article-recent-content">
												<h5>
													<a href="#">Citizenship</a>
												</h5>
												<h3 class="headline-third">
													<a href="#">Welcome to Active Citizen, we're glad you're ...</a>
												</h3>
												<div class="article-recent-entry">
													<p>We hope you like our new home.</p>
												</div>
												<div class="article-recent-meta">
													<p></p>
													<p class="list_of_authors">
														<span>by&nbsp;</span><span>
														<a href="#" style="display: inline;">Nathaniel Olugbogi</a></span>
													</p>Sept. 26, 2016
				        							<p></p>
				        						</div>
			        						</div>
		        						</li>
	        						</ol>
								</div>
								<div class="col-md-3">
									<div class="points-per-action">
										<p>
											<span><strong>1point</strong></span> earned
								      	</p>
							      	</div>
								</div>
							</div>
							<!--recent action list to be repeated as action increases ends here-->
						</div>
					</div>
				</div>
			</section>
			<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGN ETC BAR END HERE
			========================================================================-->
@endsection