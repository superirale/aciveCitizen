@extends('layouts.master')
@section('content')
	<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGNS ETC BAR BEGIN HERE
			========================================================================-->
			<section class="content-area clearfix">
				<div class="jumbotron" style="height: 250px; margin-bottom: 30px;">

				    
				</div>
				<div class="container" id="gallery-list">
					<div class="search-container">
						<form>
							<span class="fa fa-search fa-3x"></span>
							<input type="text" class="search" name="search" placeholder="Search.."/>
						</form>
					</div>
					<div class="row list">
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/cake.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/baby.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/image.jfif">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/jumia.jfif">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/nice.jfif">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/spar.jfif">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/easy.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/book.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/cinema.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/gym.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/laundry.png">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/shoe.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/smile.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/spa.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/star.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/wash.png">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/wine.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/car.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/cloth.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/cup.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/dish.png">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/girl.png">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/park.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<div class="col-md-12 reward-container">
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/example.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">2 tickets to Lissie in Philadelphia on October 10, 2016</a>
											</h3>
											<p>
												<a href="#">World Cafe Live in Philadelphia </a>
											</p>
											<div class="story-reward">
												<a href="reward-detail.html" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="pagination-container">
						<ul class="pagination"></ul>
					</div>
				</div>
			</section>
			<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGN ETC BAR END HERE
			========================================================================-->
@endsection