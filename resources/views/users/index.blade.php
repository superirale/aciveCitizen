@extends('layouts.master')

@section('content')

<section class="content-area clearfix">
				<div style="height: 250px; margin-bottom: 30px;">
					<!-- Swiper -->
				    <div class="swiper-container">
				        <div class="swiper-wrapper">
				            <div class="swiper-slide">Slide 1</div>
				            <div class="swiper-slide">Slide 2</div>
				            <div class="swiper-slide">Slide 3</div>
				            <div class="swiper-slide">Slide 4</div>
				            <div class="swiper-slide">Slide 5</div>
				            <div class="swiper-slide">Slide 6</div>
				            <div class="swiper-slide">Slide 7</div>
				            <div class="swiper-slide">Slide 8</div>
				            <div class="swiper-slide">Slide 9</div>
				            <div class="swiper-slide">Slide 10</div>
				        </div>
				        <!-- Add Pagination -->
				        <div class="swiper-pagination"></div>
				       <!-- Add Arrows -->
				       <div class="swiper-button-next swiper-button-white" style="color:#fff;"></div>
				       <div class="swiper-button-prev swiper-button-white" style="color:#fff;"></div>
				    </div>
				</div>
				<div class="container">
					<div class="row">
						<div class="col-md-12 gallery">
							<div class="row is-flex" style="padding-left: 15px; padding-right: 15px;">
								<div class="col-md-4">
									<div class="col-md-12 content-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<img src="/users/img/example.jpg">
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h6>Army Justin</h6>
											<div class="story">
												<a href="#" class="btn btn-custom">More</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="col-md-12 content-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<img src="/users/img/example.jpg">
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h6>Army Justin</h6>
											<div class="story">
												<a href="#" class="btn btn-custom">More</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="col-md-12 content-gallery" style="height: 100%;">
										<div class="col-md-12 story-image">
											<h3 style="margin-top: 10px;">NEWSLETTER</h3>
											<p>Sign up to receive alerts about world's biggest chalenges testtest </p>
											<div class="form-group" style="padding-top: 15px">
									            <input type="email" class="form-control" placeholder="someone@something.com" required id="mc-email" style="height: 50px; z-index: initial;">
									        </div>
										</div>
										<div class="col-md-12 story-description">
											<div class="" style="text-align: center; margin-top: 50px;">
												<a href="#" class="btn btn-black">SUBMIT <i class="fa fa-envelope"></i> </a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery">
							<h3 class="gallery-header">Campaign</h3>
							<div class="col-md-12 gallery-container">
								<div class="col-md-4">
									<div class="col-md-12 content-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<img src="/users/img/example.jpg">
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h6>Army Justin</h6>
											<div class="story">
												<a href="#" class="btn btn-custom">More</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="col-md-12 content-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<img src="/users/img/example.jpg">
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h6>Army Justin</h6>
											<div class="story">
												<a href="#" class="btn btn-custom">More</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="col-md-12 content-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<img src="/users/img/example.jpg">
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h6>Army Justin</h6>
											<div class="story">
												<a href="#" class="btn btn-custom">More</a>
											</div>
										</div>
									</div>
								</div>
								<!--view more button -->
								<div class="col-md-12 button-container">
									<div class="col-md-4 col-md-offset-4">
										<a type="button" class="btn btn-custom btn-block" href="#">View more campaign</a>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12 gallery" style="margin-bottom: 20px;">
							<h3 class="gallery-header">Rewards</h3>
							<div class="col-md-12 gallery-container">
								<div class="col-md-4">
									<div class="col-md-12 content-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<img src="/users/img/example.jpg">
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h6>Army Justin</h6>
											<div class="story">
												<a href="#" class="btn btn-custom">More</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="col-md-12 content-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<img src="/users/img/example.jpg">
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h6>Army Justin</h6>
											<div class="story">
												<a href="#" class="btn btn-custom">More</a>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="col-md-12 content-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<img src="/users/img/example.jpg">
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h6>Army Justin</h6>
											<div class="story">
												<a href="#" class="btn btn-custom">More</a>
											</div>
										</div>
									</div>
								</div>
								<!--view more button -->
								<div class="col-md-12 button-container">
									<div class="col-md-4 col-md-offset-4">
										<a type="button" class="btn btn-custom btn-block" href="#">View more Rewards</a>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</section>
			<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGN ETC BAR END HERE
			========================================================================-->
			<!--=============================================================================
				MODAL POP UP
		==============================================================================-->
		<div class="modal fade" id="signInModal">
		    <div class="modal-dialog" style="width: 65.88078%;">
		        <div class="modal-content clearfix">
		            <div class="modal-header" style="text-align: center; border-bottom: none;">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <img src="/users/img/logo.png" style="max-width: 15%; margin-top: 50px;" />
		            </div>
		            <div class="modal-body clearfix">
		            	<div class="col-md-6" style="padding: 20px;">
		            		<h5>Sign in with Facebook</h5>
		            		<br/>
		            		<a href="{{ url('auth/facebook') }}" class="btn btn-block btn-facebook">
		            			<i class="fa fa-facebook"></i> Sign in with Facebook
		            		</a>
		            	</div>
		            	<div class="col-md-6" style="padding: 20px; border-left: 1px solid #ddd">
			            	<p>Or sign in with email</p>
		            		<form action="{{ url('/login') }}" method="POST" id="myForm">
		            				{{ csrf_field() }}
								<div class="form-group">
								    <label for="email">Email address:</label>
								    <input type="email" name="email" class="form-control" id="email" placeholder="joseph@example.com">
								   <div class="error" id="error-email" style="display:none; color: red;">
								        Email format is incorrect
								    </div>
								</div>
								<div class="form-group">
								    <label for="pwd">Password:</label>
								    <input type="password" class="form-control" id="pwd" placeholder="password" name="password">
								</div>
								<div class="checkbox">
								    <label><input type="checkbox" name="remember"> Remember me</label>
								</div>
								<div id="loading" style="display: none; margin: 0 auto; width: 40px; height: 40px; background: url(/users/img/Loaders/loader7.gif) center no-repeat #fff; transition: all ease 1s">
								</div>
								<div style="text-align: center;">
									<a type="submit" class="btn btn-black" onclick="validateForm()" style="width: 80%; margin: 0 auto;">Sign In</a><br><br>
								</div>
							</form>
							<a href="#" class="btn btn-outline">Forgot your password</a>
						</div>
		            </div>
		        </div>
		    </div>
		</div>

		<div class="modal fade" id="signUpModal">
		    <div class="modal-dialog" style="width: 65.88078%; text-align: center; font-size: 15px;">
		        <div class="modal-content clearfix">
		            <div class="modal-header"  style="text-align: center; border-bottom: none;">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		                <img src="/users/img/logo.png" style="max-width: 15%; margin-top: 50px;" />
		            </div>
		            <div class="modal-body clearfix">
		            	<div class="col-md-8 col-md-offset-2" style="padding: 20px;">

		            		<div class="col-md-12">
		            			<p>It all starts with becoming a Global Citizen.</p>
			            		<a href="{{ url('auth/facebook') }}" class="btn btn-block btn-facebook">
			            			<i class="fa fa-facebook"></i> Sign up with Facebook
			            		</a>
			            		<br/>
			            		<div class="horizontal-rule-with-text">
			            			<span>OR</span>
			            		</div>
			            		<br/>
			            		<br/>
		            		</div>

		            		<div class="col-md-12">
		            			<form>
									<div class="form-group">
									    <input type="email" class="form-control" id="email" placeholder="Email Address">
									</div>
								</form>
								<br/>
								<a href="#" class="btn btn-block btn-custom">
			            			<i class="fa fa-envelope"></i> Sign up with Email
			            		</a>
			            		<br/>
			            		<p>By signing up, I agree to Active Citizen's <a href="#" class="inline">Terms of Service.</a></p>
			            		<p style="padding-top: 16px; margin-top: 16px; border-top: 1px solid #bebebe;">Already have an account? <a href="#" class="inline" data-toggle="modal" data-target="#signInModal" data-dismiss="modal">Log In</a></p>
		            		</div>
		            		 

		            	</div>
		            </div>
		        </div>
		    </div>
		</div>

		<!--=============================================================================
				MODAL POP UP
		==============================================================================-->





@endsection