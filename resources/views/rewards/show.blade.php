@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">reward {{ $reward->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('rewards/' . $reward->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit reward"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['rewards', $reward->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete reward',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $reward->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $reward->title }} </td></tr><tr><th> Reward Time </th><td> {{ $reward->reward_time }} </td></tr><tr><th> Reward Date </th><td> {{ $reward->reward_date }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection