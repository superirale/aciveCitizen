<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('reward_time') ? 'has-error' : ''}}">
    {!! Form::label('reward_time', 'Reward Time', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('time', 'reward_time', null, ['class' => 'form-control']) !!}
        {!! $errors->first('reward_time', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('reward_date') ? 'has-error' : ''}}">
    {!! Form::label('reward_date', 'Reward Date', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::date('reward_date', null, ['class' => 'form-control']) !!}
        {!! $errors->first('reward_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('reward_image_reference') ? 'has-error' : ''}}">
    {!! Form::label('reward_image_reference', 'Reward Image Reference', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('reward_image_reference', null, ['class' => 'form-control']) !!}
        {!! $errors->first('reward_image_reference', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('points') ? 'has-error' : ''}}">
    {!! Form::label('points', 'Points', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('points', null, ['class' => 'form-control']) !!}
        {!! $errors->first('points', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('location') ? 'has-error' : ''}}">
    {!! Form::label('location', 'Location', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('location', null, ['class' => 'form-control']) !!}
        {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>