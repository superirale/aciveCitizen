@extends('layouts.master')
@section('content')
	<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGNS ETC BAR BEGIN HERE
			========================================================================-->
			<section class="content-area clearfix">
				<div class="jumbotron" style="height: 250px; margin-bottom: 30px;">

				    
				</div>
				<div class="container" id="gallery-list">
					<div class="search-container">
						<form>
							<span class="fa fa-search fa-3x"></span>
							<input type="text" class="search" name="search" placeholder="Search.."/>
						</form>
					</div>
					<div class="row list">
						<div class="col-md-12 gallery">
						
							<div class="col-md-12 reward-container">
							@foreach($rewards as $reward)
							
							
								<div class="col-md-4 reward-border">
									<div class="col-md-12 reward-gallery">
										<div class="col-md-12 story-image">
											<div class="thumbnail">
												<a href="#">
													<img src="/users/img/cake.jpg">
												</a>
											</div>
										</div>
										<div class="col-md-12 story-description">
											<h3>
												<a href="#">{{$reward->title}}</a>
											</h3>
											<p>
												<a href="#">{{$reward->location}}</a>
											</p>
											<div class="story-reward">
												<a href="" class="btn btn-black">View Details</a>
											</div>
										</div>
									</div>
								</div>
								
								
								
									@endforeach	
							</div>
							
						</div>
						
						
					</div>
					<div class="pagination-container">
						<ul class="pagination"></ul>
					</div>
				</div>
			</section>
			<!--=========================================================================
				MAIN CONTENT AND ALL IT CONTENTS SECTIONS, CAMPAIGN ETC BAR END HERE
			========================================================================-->
@endsection