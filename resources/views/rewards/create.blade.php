<!DOCTYPE html>

<html class="no-js sidebar-large">

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>ACTIVE CITIZEN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="/dashboard/assets/img/A.png">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="/dashboard/assets/css/icons/icons.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/plugins.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/dante-editor.css">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/jquery.circliful.css">
    <link href="/dashboard/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/main.css">
    <link rel="stylesheet" href="/dashboard/assets/css/simple-line-icons/css/simple-line-icons.css">
    <!-- END  MANDATORY STYLE -->
    <script src="/dashboard/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>


<body data-page="blank_page">
    <!-- BEGIN TOP MENU -->
    @include('includes.menu')
    <!-- END TOP MENU -->
    <!-- BEGIN WRAPPER -->
    <div id="wrapper">
        <!-- BEGIN MAIN SIDEBAR -->
        @include('includes.sidebar')
        <!-- END MAIN SIDEBAR -->
        <!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Add Rewards</h1>
                </div>
            </div>
            <div class="row">
                <div class="content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="card clearfix">
                                    <div class="header">
                                        <h4 class="title">Reward Details</h4>

                                    </div>
                                    <div class="content clearfix">
                                        <form>
                                            <div class="form-group">
                                                <label for="text">Reward Title:</label>
                                                <input type="text" class="form-control" id="reward_title">
                                            </div>
                                            <div class="form-group">
                                                <label for="number">Point(s):</label>
                                                <input type="number" class="form-control" id="reward_point">
                                            </div>
                                            <div class="form-group">
                                                <label for="text">Location(s):</label>
                                                <input type="text" class="form-control" id="reward_location">
                                            </div>
                                            <div class="form-group">
                                                <label for="text">Time:</label>
                                                <input type="number" class="form-control" id="reward_time">
                                            </div>
                                            <div class="form-group">
                                                <label for="text">Date:</label>
                                                <input type="text" class="form-control" id="reward_date">
                                            </div>

                                            <a type="button" class="btn btn-custom" id="submit_button">Submit</a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <!--================================================================
                                cover/brand for reward
                                =================================================================-->
                                <div class="picture-container">
                                    <div class="picture">
                                        <img src="" class="picture-src" id="wizardPicturePreview"/>
                                        <input type="file" id="wizard-picture" accept="image/*"/>
                                    </div>
                                </div>
                                <!--================================================================
                                cover/brand for reward ends here
                                =================================================================-->
                            </div>
                            <div class="col-md-3 reward_clone" style="display: none;">
                                <div class="card clearfix">
                                    <a href="#" class="editReward" data-dismiss="modal">
                                        <div class="header">
                                            <h4 class="title">A ticket for Five at Chinese Restuarant</h4>
                                            <p class="category">
                                                <span class="points">5</span> points
                                            </p>
                                        </div>
                                        <div class="content clearfix">
                                            <div class="thumbnail">
                                                <img src="/dashboard/assets/img/rewards/book.jpg">
                                            </div>
                                            <div class="footer">
                                                <hr>
                                                <div class="stats">
                                                    <i class="fa fa-history"></i> 3 minutes ago
                                                </div>
                                            </div>
                                        </div>
                                        <div class="textbox">
                                            <div class="textContent">
                                                <p class="text">
                                                    <i class="icon-calendar"></i>
                                                    <span class="dates">Nov, 8 2016</span>
                                                </p>
                                                <p class="text">
                                                    <i class="icon-location-pin"></i>
                                                    <span class="locations">Onikan Event Centre</span>
                                                </p>
                                                <h3><i class="icon-globe"></i></h3>
                                                <p class="text">View Details</p>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END WRAPPER -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="/dashboard/assets/plugins/jquery-1.11.js"></script>
    <script src="/dashboard/assets/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="/dashboard/assets/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery-mobile/jquery.mobile-1.4.2.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-select/bootstrap-select.js"></script>
    <script src="/dashboard/assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/dashboard/assets/plugins/mmenu/js/jquery.mmenu.min.all.js"></script>
    <script src="/dashboard/assets/plugins/nprogress/nprogress.js"></script>
    <script src="/dashboard/assets/plugins/charts-sparkline/sparkline.min.js"></script>
    <script src="/dashboard/assets/plugins/breakpoints/breakpoints.js"></script>
    <script src="/dashboard/assets/plugins/numerator/jquery-numerator.js"></script>
    <script src="/dashboard/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <!--  Charts Plugins -->
    <script src="/dashboard/assets/plugins/chartist.min.js"></script>
    <script src="/dashboard/assets/plugins/Chart.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.circliful.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.flot.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.flot.animator.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.flot.resize.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery.flot.time.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-notify.js"></script>
    <script src="/dashboard/assets/plugins/waypoint.js"></script>
    <script src="/dashboard/assets/plugins/countUp.js"></script>
    <!-- END MANDATORY SCRIPTS -->
    <script src="/dashboard/assets/js/application.js"></script>
    <script src="/dashboard/assets/js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){

             app.readFile();
             app.rewardsHandler();
             app.searchToggler();

        });
    </script>
</body>
</html>
