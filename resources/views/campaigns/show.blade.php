@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">campaign {{ $campaign->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('campaigns/' . $campaign->id . '/edit') }}" class="btn btn-primary btn-xs" title="Edit campaign"><span class="glyphicon glyphicon-pencil" aria-hidden="true"/></a>
                        {!! Form::open([
                            'method'=>'DELETE',
                            'url' => ['campaigns', $campaign->id],
                            'style' => 'display:inline'
                        ]) !!}
                            {!! Form::button('<span class="glyphicon glyphicon-trash" aria-hidden="true"/>', array(
                                    'type' => 'submit',
                                    'class' => 'btn btn-danger btn-xs',
                                    'title' => 'Delete campaign',
                                    'onclick'=>'return confirm("Confirm delete?")'
                            ))!!}
                        {!! Form::close() !!}
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $campaign->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $campaign->title }} </td></tr><tr><th> Description </th><td> {{ $campaign->description }} </td></tr><tr><th> Image Reference </th><td> {{ $campaign->image_reference }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection