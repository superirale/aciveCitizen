<!DOCTYPE html>

<html class="no-js sidebar-large">

<head>
    <!-- BEGIN META SECTION -->
    <meta charset="utf-8">
    <title>ACTIVE CITIZEN</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta content="" name="description" />
    <meta content="themes-lab" name="author" />
    <link rel="shortcut icon" href="/dashboard/assets/img/A.png">
    <!-- END META SECTION -->
    <!-- BEGIN MANDATORY STYLE -->
    <link href="/dashboard/assets/css/icons/icons.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/dashboard/assets/css/plugins.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/dashboard/assets/css/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/dante-editor.css">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/jquery.circliful.css">
    <link href="/dashboard/assets/css/style.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/dashboard/assets/css/main.css">

    <!-- END  MANDATORY STYLE -->
    <script src="/dashboard/assets/plugins/modernizr/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>


<body data-page="add_campaign">
    <!-- BEGIN TOP MENU -->
    @include('includes.menu')
    <!-- END TOP MENU -->
    <!-- BEGIN WRAPPER -->
    <div id="wrapper">
        @include('includes.sidebar')
        <!-- BEGIN MAIN CONTENT -->
        <div id="main-content">
            <div class="row">
                <div class="col-lg-12">
                    <h1 id="campaign_title">Add Campaign(s)</h1>
                </div>
            </div>

            <div class="row">

                    <div class="content">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="page-notes">
                                        <div class="content clearfix">
                                            <div class="">
                                                <!--================================================================
                                                cover/brand for campaign
                                                =================================================================-->
                                                <div class="picture-container">
                                                    <div class="picture">
                                                        {{--
                                                        <?php
                                                            $image_path =  public_path('upload/campaigns/'. md5($campaign->id).'/'.$campaign->image_reference);
                                                            $type = pathinfo($image_path, PATHINFO_EXTENSION);
                                                            $data = file_get_contents($image_path);
                                                            $base64Image = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                                             //dd($image_path);

                                                        ?> --}}
                                                        <img src="{{$base64Image}}" class="picture-src" id="wizardPicturePreview"/>
                                                        <input type="file" id="wizard-picture" accept="image/*"/>
                                                    </div>
                                                </div>
                                                <!--================================================================
                                                cover/brand for campaign ends here
                                                =================================================================-->
                                                <div class="story-container">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="col-md-12 card">
                                                                <h4>
                                                                    Take Action
                                                                </h4>
                                                                <div class="clearfix">

                                                                    <div class="cloneContainer">
                                                                        <!-- twitter section -->
                                                                        <div class='clone clearfix animated' data='twitter' style="display: none;">
                                                                            <a href="javascript:void(0);" class="remove">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>
                                                                            <h5>Twitter</h5>
                                                                            <div class='col-md-2'>
                                                                                <img src='/dashboard/assets/img/avatar.png'>
                                                                            </div>
                                                                            <div class='col-md-10'>

                                                                                    <div class='form-group'>
                                                                                      <label for=''>Instructions</label>
                                                                                      <textarea name="instructions" class='form-control' rows='5' placeholder="some text"></textarea>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                      <label for=''>Tweet</label>
                                                                                      <textarea name="message" class='form-control' rows='5'  placeholder="some text"></textarea>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                        <div class='input-group'>
                                                                                          <div class='input-group-addon'>#</div>
                                                                                          <input name="hashtag" type='text' class='form-control' placeholder='Add hash tag'>
                                                                                        </div>
                                                                                    </div>
                                                                                    <a type='button' class='btn btn-danger'>
                                                                                        <i class='fa fa-plus'></i> Submit
                                                                                    </a>

                                                                            </div>
                                                                        </div>
                                                                        @foreach($campaign->campaignActions as $cAction)
                                                                            @if($cAction->action->name == "facebook")
                                                                                <!--facebook section -->
                                                                                <div class='clone clearfix animated fadeIn' data='facebook' style="display: block;">
                                                                                    <a href="javascript:void(0);" class="remove">
                                                                                        <i class="fa fa-times"></i>
                                                                                    </a>
                                                                                    <h5>Facebook</h5>
                                                                                    <div class='col-md-2'>
                                                                                        <img src='/dashboard/assets/img/avatar.png'>
                                                                                    </div>
                                                                                    <div class='col-md-10'>
                                                                                            <div class='form-group'>
                                                                                              <label for=''>Instructions</label>
                                                                                              <textarea name="instructions" class='form-control' rows='5' placeholder="some text">{{$cAction->instructions}}</textarea>
                                                                                            </div>
                                                                                            <div class='form-group'>
                                                                                              <label for=''>Message</label>
                                                                                              <textarea name="message" class='form-control' rows='5' placeholder="some text">{{$cAction->message}}</textarea>
                                                                                            </div>
                                                                                            <div class='form-group'>
                                                                                                <div class='input-group'>
                                                                                                  <div class='input-group-addon'>#</div>
                                                                                                  <input name="hashtag" type='text' value="{{$cAction->hashtags}}" class='form-control' placeholder='Add hash tag'>
                                                                                                </div>
                                                                                            </div>
                                                                                            <a type='button' class='btn btn-danger'>
                                                                                                <i class='fa fa-plus'></i> Submit
                                                                                            </a>

                                                                                    </div>
                                                                                </div>

                                                                            @elseif($cAction->action->name == "twitter")
                                                                                <div class='clone clearfix animated fadeIn' data='twitter' style="display: block;">
                                                                                    <a href="javascript:void(0);" class="remove">
                                                                                        <i class="fa fa-times"></i>
                                                                                    </a>
                                                                                    <h5>Twitter</h5>
                                                                                    <div class='col-md-2'>
                                                                                        <img src='/dashboard/assets/img/avatar.png'>
                                                                                    </div>
                                                                                    <div class='col-md-10'>
                                                                                        <form method="post">

                                                                                            <div class='form-group'>
                                                                                              <label for=''>Instructions</label>
                                                                                              <textarea name="instructions" class='form-control' rows='5' placeholder="some text">{{$cAction->instructions}}</textarea>
                                                                                            </div>
                                                                                            <div class='form-group'>
                                                                                              <label for=''>Tweet</label>
                                                                                              <textarea name="message" class='form-control' rows='5'  placeholder="some text">{{$cAction->message}}</textarea>
                                                                                            </div>
                                                                                            <div class='form-group'>
                                                                                                <div class='input-group'>
                                                                                                  <div class='input-group-addon'>#</div>
                                                                                                  <input name="hashtag" type='text' value="{{$cAction->hashtags}}" class='form-control' placeholder='Add hash tag'>
                                                                                                </div>
                                                                                            </div>
                                                                                            <a type='Submit' class='btn btn-danger'>
                                                                                                <i class='fa fa-plus'></i> Submit
                                                                                            </a>
                                                                                            </form>

                                                                                    </div>
                                                                                </div>
                                                                            @elseif($cAction->action->name == "petition")
                                                                                    <div class='clone clearfix animated' data='twitter' style="display: none;">
                                                                                    <a href="#" class="remove">
                                                                                        <i class="fa fa-times"></i>
                                                                                    </a>
                                                                                    <h5>Petition</h5>
                                                                                    <div class='col-md-2'>
                                                                                        <img src='/dashboard/assets/img/avatar.png'>
                                                                                    </div>
                                                                                    <div class='col-md-10'>

                                                                                            <div class='form-group'>
                                                                                              <label for=''>Instructions</label>
                                                                                              <textarea name="instructions" class='form-control' rows='5' placeholder="Sign this petition to ask PM Justin Trudeau and Minister Marie-Claude Bibeau to help families in the world’s toughest places become #hungerfree."></textarea>
                                                                                            </div>
                                                                                            <div class='form-group'>
                                                                                              <label for=''>Petition</label>
                                                                                              <textarea name="message" class='form-control' rows='5' placeholder="As Canadians show leadership in fighting global hunger for those most in need, I ask you, Prime Minister Justin Trudeau and Minister Marie Claude Bibeau, to:"></textarea>
                                                                                            </div>
                                                                                            <a type='button' class='btn btn-custom'>
                                                                                                <i class='fa fa-plus'></i> Submit
                                                                                            </a>

                                                                                    </div>
                                                                                </div>
                                                                            @endif
                                                                        @endforeach
                                                                        <!--facebook section -->
                                                                        <div class='clone clearfix animated' data='facebook' style="display: none;">
                                                                            <a href="javascript:void(0);" class="remove">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>
                                                                            <h5>Facebook</h5>
                                                                            <div class='col-md-2'>
                                                                                <img src='/dashboard/assets/img/avatar.png'>
                                                                            </div>
                                                                            <div class='col-md-10'>

                                                                                    <div class='form-group'>
                                                                                      <label for=''>Instructions</label>
                                                                                      <textarea name="instructions" class='form-control' rows='5' placeholder="some text"></textarea>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                      <label for=''>Message</label>
                                                                                      <textarea name="message" class='form-control' rows='5' placeholder="some text"></textarea>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                        <div class='input-group'>
                                                                                          <div class='input-group-addon'>#</div>
                                                                                          <input name="hashtag" type='text' class='form-control' placeholder='Add hash tag'>
                                                                                        </div>
                                                                                    </div>
                                                                                    <a type='button' class='btn btn-danger'>
                                                                                        <i class='fa fa-plus'></i> Submit
                                                                                    </a>

                                                                            </div>
                                                                        </div>
                                                                        <!-- instagram section -->
                                                                        <div class='clone clearfix animated' data='twitter' style="display: none;">
                                                                            <a href="javascript:void(0);" class="remove">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>
                                                                            <h5>Instagram</h5>
                                                                            <div class='col-md-2'>
                                                                                <img src='/dashboard/assets/img/avatar.png'>
                                                                            </div>
                                                                            <div class='col-md-10'>

                                                                                    <div class='form-group'>
                                                                                      <label for=''>Instructions</label>
                                                                                      <textarea name="instructions" class='form-control' rows='5' placeholder="some text"></textarea>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                        <div class='input-group'>
                                                                                          <div  class='input-group-addon'>#</div>
                                                                                          <input name="hashtag" type='text' class='form-control' placeholder='Add hash tag'>
                                                                                        </div>
                                                                                    </div>
                                                                                    <a type='button' class='btn btn-danger'>
                                                                                        <i class='fa fa-plus'></i> Submit
                                                                                    </a>

                                                                            </div>
                                                                        </div>
                                                                        <!-- google section -->
                                                                        <div class='clone clearfix animated' data='twitter' style="display: none;">
                                                                            <a href="#" class="remove">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>
                                                                            <h5>Petition</h5>
                                                                            <div class='col-md-2'>
                                                                                <img src='/dashboard/assets/img/avatar.png'>
                                                                            </div>
                                                                            <div class='col-md-10'>

                                                                                    <div class='form-group'>
                                                                                      <label for=''>Instructions</label>
                                                                                      <textarea name="instructions" class='form-control' rows='5' placeholder="Sign this petition to ask PM Justin Trudeau and Minister Marie-Claude Bibeau to help families in the world’s toughest places become #hungerfree."></textarea>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                      <label for=''>Petition</label>
                                                                                      <textarea name="message" class='form-control' rows='5' placeholder="As Canadians show leadership in fighting global hunger for those most in need, I ask you, Prime Minister Justin Trudeau and Minister Marie Claude Bibeau, to:"></textarea>
                                                                                    </div>
                                                                                    <a type='button' class='btn btn-custom'>
                                                                                        <i class='fa fa-plus'></i> Submit
                                                                                    </a>

                                                                            </div>
                                                                        </div>
                                                                        <!-- email section -->
                                                                        <div class='clone clearfix animated' data='twitter' style="display: none;">
                                                                            <a href="javascript:void(0);" class="remove">
                                                                                <i class="fa fa-times"></i>
                                                                            </a>
                                                                            <h5>Email</h5>
                                                                            <div class='col-md-2'>
                                                                                <img src='/dashboard/assets/img/avatar.png'>
                                                                            </div>
                                                                            <div class='col-md-10'>

                                                                                    <div class='form-group'>
                                                                                      <label for=''>Instructions</label>
                                                                                      <textarea name="instructions" class='form-control' rows='5' placeholder="some text"></textarea>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                        <div class='input-group'>
                                                                                          <div class='input-group-addon'>Subject</div>
                                                                                          <input name="subject" type='text' class='form-control' placeholder='Subject'>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                        <div class='input-group'>
                                                                                          <div class='input-group-addon'>To</div>
                                                                                          <input name="to" type='text' class='form-control' placeholder='to recipient'>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class='form-group'>
                                                                                      <label for=''>Message</label>
                                                                                      <textarea name="message" class='form-control' rows='5' placeholder="some text"></textarea>
                                                                                    </div>

                                                                                    <a type='button' class='btn btn-custom'>
                                                                                        <i class='fa fa-plus'></i> Submit
                                                                                    </a>

                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="social-icons">
                                                                        <ul class="activity animated icons-list" style="display: none;">
                                                                        @foreach($actions as $action)
                                                                           <li>
                                                                               <a href="javascript:void(0);" class="{{$action->name}}">
                                                                                    <p class="inline">{{$action->name}}</p>
                                                                                    @if($action->name == 'email')
                                                                                    <i class="fa fa-envelope"></i>
                                                                                    @elseif($action->name == 'petition')
                                                                                    <i class="fa fa-product-hunt" aria-hidden="true"></i>
                                                                                    @else
                                                                                    <i class="fa fa-{{$action->name}}"></i>
                                                                                    @endif
                                                                                </a>
                                                                           </li>
                                                                           @endforeach
                                                                        </ul>
                                                                        <ul class="activity">
                                                                            <li>
                                                                               <a href="javascript:void(0)" class ="showIcons">
                                                                                    <i class="fa fa-plus animated infinite shake"></i>
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="col-md-12 card">
                                                                <div id="editor"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 m-t-20 m-b-40 align-center">
                                                    <a href="campaign.html" class="btn btn-default m-r-10 m-t-10"><i class="fa fa-reply"></i> Cancel</a>
                                                    <a href="#" class="btn btn-custom m-t-10" id="publish"><i class="fa fa-check"></i> Save &amp; Publish</a>
                                                   {{--  <button class="btn btn-custom m-t-10"> <i class="fa fa-check"></i> Save &amp; Publish</button> --}}
                                                </div>
                                                <!--end of row-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

            </div>
        </div>
        <!-- END MAIN CONTENT -->
    </div>
    <!-- END WRAPPER -->
    <!-- BEGIN MANDATORY SCRIPTS -->
    <script src="/dashboard/assets/plugins/jquery-1.11.js"></script>
    <script src="/dashboard/assets/plugins/jquery-migrate-1.2.1.js"></script>
    <script src="/dashboard/assets/plugins/jquery-ui/jquery-ui-1.10.4.min.js"></script>
    <script src="/dashboard/assets/plugins/jquery-mobile/jquery.mobile-1.4.2.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script src="/dashboard/assets/plugins/bootstrap-select/bootstrap-select.js"></script>
    <script src="/dashboard/assets/plugins/mcustom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
    <script src="/dashboard/assets/plugins/mmenu/js/jquery.mmenu.min.all.js"></script>
    <script src="/dashboard/assets/plugins/nprogress/nprogress.js"></script>
    <script src="/dashboard/assets/plugins/charts-sparkline/sparkline.min.js"></script>
    <script src="/dashboard/assets/plugins/breakpoints/breakpoints.js"></script>
    <script src="/dashboard/assets/plugins/numerator/jquery-numerator.js"></script>
    <script src="/dashboard/assets/plugins/jquery.cookie.min.js" type="text/javascript"></script>
    <!--  Charts Plugins -->
    <script src="/dashboard/assets/plugins/underscore-min.js"></script>
    <script src="/dashboard/assets/plugins/sanitize.js"></script>
    <script src="/dashboard/assets/plugins/dante-editor.js"></script>
    <script src="/dashboard/assets/plugins/jquery.dataTables.min.js"></script>
    <script src="/dashboard/assets/plugins/dataTables.bootstrap.js"></script>
    <script src="/dashboard/assets/plugins/dataTables.tableTools.js"></script>
    <script src="/dashboard/assets/plugins/moment.min.js"></script>
    <script src="/dashboard/assets/plugins/daterangepicker.js"></script>
    <!-- END MANDATORY SCRIPTS -->
    <script src="/dashboard/assets/js/application.js"></script>
    <script src="/dashboard/assets/js/main.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            app.mediumHandler();
            app.readFile();
            app.selectPicker();
            app.toggleIconsHandler();
            app.clonesHandler();
            app.campaignsHandler();
            app.searchToggler();
        });
        $(document).on('ready', function(){
            $("#editor").find('h3').text("{{$campaign->title}}");
            $("#editor").find('p').text("{{{$campaign->description}}}");

        });
    </script>
</body>
</html>
