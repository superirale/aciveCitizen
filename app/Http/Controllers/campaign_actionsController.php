<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CampaignAction;
use Illuminate\Http\Request;
use Session;

class campaign_actionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $campaign_actions = CampaignAction::paginate(25);

        return view('campaign_actions.index', compact('campaign_actions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('campaign_actions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        // dd($request->all());
        $requestData = $request->all();

        campaign_action::create($requestData);

        Session::flash('flash_message', 'campaign_action added!');

        return redirect('campaign_actions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $campaign_action = campaign_action::findOrFail($id);

        return view('campaign_actions.show', compact('campaign_action'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $campaign_action = campaign_action::findOrFail($id);

        return view('campaign_actions.edit', compact('campaign_action'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $campaign_action = campaign_action::findOrFail($id);
        $campaign_action->update($requestData);

        Session::flash('flash_message', 'campaign_action updated!');

        return redirect('campaign_actions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        campaign_action::destroy($id);

        Session::flash('flash_message', 'campaign_action deleted!');

        return redirect('campaign_actions');
    }
}
