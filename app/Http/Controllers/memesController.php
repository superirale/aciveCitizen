<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Meme;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Helpers\Uploader;

class memesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $memes = Meme::where('user_id', Auth::user()->id)->get();

        return view('memes.index', compact('memes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('memes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $requestData = $request->except('image_reference');
        $requestData['image_reference'] = '';
        $requestData['user_id'] = Auth::user()->id;
        $meme = Meme::create($requestData);

        if($request->has('image_reference')){
            $img = str_replace('data:image/png;base64,', '', $request->image_reference);
            $img = str_replace(' ', '+', $img);
            $data = base64_decode($img);

            $uploader = new Uploader();
            $main_dir = env("UPLOAD_DIR").'/memes/';
            $upload_dir = $uploader->getUploadDir($meme->id, $main_dir);
            $filename = uniqid().$meme->id.".png";
            $file = $upload_dir.'/'. $filename;
            $success = file_put_contents($file, $data);

            $updated_meme = Meme::find($meme->id);
            $updated_meme->image_reference = $filename;
            $updated_meme->save();

        }

        Session::flash('flash_message', 'meme added!');

        return response()->json(['status' => 'done']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $meme = Meme::findOrFail($id);

        return view('memes.show', compact('meme'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $meme = Meme::findOrFail($id);

        return view('memes.edit', compact('meme'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $meme = Meme::findOrFail($id);
        $meme->update($requestData);

        Session::flash('flash_message', 'meme updated!');

        return redirect('memes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Meme::destroy($id);

        Session::flash('flash_message', 'meme deleted!');

        return redirect('memes');
    }
}
