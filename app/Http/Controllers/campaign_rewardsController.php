<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\campaign_reward;
use Illuminate\Http\Request;
use Session;

class campaign_rewardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $campaign_rewards = campaign_reward::paginate(25);

        return view('campaign_rewards.index', compact('campaign_rewards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('campaign_rewards.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        campaign_reward::create($requestData);

        Session::flash('flash_message', 'campaign_reward added!');

        return redirect('campaign_rewards');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $campaign_reward = campaign_reward::findOrFail($id);

        return view('campaign_rewards.show', compact('campaign_reward'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $campaign_reward = campaign_reward::findOrFail($id);

        return view('campaign_rewards.edit', compact('campaign_reward'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $campaign_reward = campaign_reward::findOrFail($id);
        $campaign_reward->update($requestData);

        Session::flash('flash_message', 'campaign_reward updated!');

        return redirect('campaign_rewards');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        campaign_reward::destroy($id);

        Session::flash('flash_message', 'campaign_reward deleted!');

        return redirect('campaign_rewards');
    }
}
