<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\action;
use App\Models\campaign;
use App\Models\CampaignAction;
use App\Models\Rewards;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Session;
use Auth;
use App\Helpers\Uploader;

class campaignsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $campaigns = Campaign::where('user_id', Auth::user()->id)->paginate(25);
        $total_campaigns = Campaign::where('user_id', Auth::user()->id)->get()->count();
        $pending_campaigns = Campaign::where('user_id', Auth::user()->id)->where('status', 'pending')->get()->count();
        $active_campaigns = Campaign::where('user_id', Auth::user()->id)->where('status', 'active')->get()->count();
        $deleted_campaigns = Campaign::where('user_id', Auth::user()->id)->whereNotNull('deleted_at')->get()->count();

        return view('campaigns.index', compact('campaigns', 'total_campaigns', 'active_campaigns', 'pending_campaigns', 'deleted_campaigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $actions = Action::all();
        return view('campaigns.create', compact('actions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $campaign_actions = json_decode($request->campaign_action_data, true);

        $requestData = $request->except('campaign_action_data');
        $requestData['user_id'] = Auth::user()->id;
        //$requestData['image_reference'] = "";

        dd($requestData);

        $campaign = Campaign::create($requestData);

        if($request->hasFile('image_reference')){

            //$uploader = new Uploader();
//            $main_dir = env("UPLOAD_DIR").'/campaigns';
            //$upload_dir = $uploader->getUploadDir($campaign->id, $main_dir);

            $extension = $request->image_reference->extension();
            $filename = uniqid().$campaign->id.'.'.$extension;

            //$uploader->upload($request->image_reference, $new_filename, '');
            Image::make($request->image_reference)->save(env("UPLOAD_DIR").'/campaigns/'.$filename);

            //update the campaign
            $campaign->image_reference = $filename;
            $campaign->save();
        }

        if(count($campaign_actions) > 0){
            $arr = [];

            foreach ($campaign_actions as $action => $value) {

                $actionObj = Action::where('name', strtolower($action))->first();
                // dd($actionObj);
                $arr[] = new CampaignAction([
                    'action_id' => $actionObj->id,
                    'message' => isset($value['message'])? $value['message'] : null,
                    'instructions' => $value['instructions'],
                    'hashtags' => $value['hashtags'],
                    'to' => isset($value['to'])? $value['to']: null,
                    'subject' => isset($value['subject'])? $value['subject']: null
                ]);
            }
            $campaign->campaignActions()->saveMany($arr);
        }
        Session::flash('flash_message', 'campaign added!');
        return response()->json(['status' => 'done']);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $campaign = Campaign::findOrFail($id);

        return view('campaigns.show', compact('campaign'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $campaign = Campaign::with(['campaignActions' => function ($q){
            $q->with('action');
        }])->findOrFail($id);
        // dd($campaign);
        $actions = Action::all();


        return view('campaigns.edit', compact('campaign','actions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $campaign = Campaign::findOrFail($id);
        $campaign->update($requestData);

        Session::flash('flash_message', 'campaign updated!');

        return redirect('campaigns');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Campaign::destroy($id);

        Session::flash('flash_message', 'campaign deleted!');

        return redirect('campaigns');
    }
}